# Projeto de ESOFT 2019-2020


# 1. Constituição do Grupo de Trabalho ###

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.	   | Nome do Aluno		   	     |
|--------------|-----------------------------|
| **1182115**  | Matheus Aguiar              |
| **1191396**  | Marcel Gavioli              |
| **1090552**  | Daniel Teixeira             |
| **1100241**  | Joel Ribeiro                |



# 2. Distribuição de Tarefas ###

A distribuição de tarefas ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito na tabela seguinte.

**Esta tabela deve estar sempre atualizada.**

| Tarefa                      | [Iteração 1](Iteracao1/README.md)   | [Iteração 2](Iteracao2/README.md) | [Iteração 3](Iteracao1/README.md) |
|-----------------------------|------------|------------|------------|
| Diagrama Casos de Uso (DUC) |  [fornecido](Iteracao1/DUC.md)      |   [todos](Iteracao2/DUC.md)                                                   |   [todos](Iteracao3/DUC.md)          |
| Glossário                   |  [todos](Iteracao1/Glossario.md)    |   [todos](Iteracao2/Glossario.md)                                             |   [todos](Iteracao3/Glossario.md)    |
| Especificação Suplementar   |   (n/a)                             |   [todos](Iteracao2/FURPS.md)                                                 |   [todos](Iteracao3/FURPS.md)        |
| Modelo de Domínio           |  [todos](Iteracao1/MD.md)           |   [todos](Iteracao2/MD.md)                                                    |   [todos](Iteracao3/MD.md)           |
| UC 1 (Requisitos + Design)  |  [fornecido](Iteracao1/UC1_RegistarOrganizacao.md)      |  [1100241](Iteracao2/UC1_RegistarOrganizacao.md)          |    -                                                      |
| UC 2 (Requisitos + Design)  |  [fornecido](Iteracao1/UC2_DefinirArea.md)              |      [Inalterado](Iteracao2/UC2_DefinirArea.md)           |    -                                                      |
| UC 3 (Requisitos + Design)  |  [1182115](Iteracao1/UC3_DefinirCategoria.md)           |      [1182115](Iteracao2/UC3_DefinirCategoria.md)         |    -                                                      |
| UC 4 (Requisitos + Design)  |  [1191396](Iteracao1/UC4_EspecificarCT.md)              |     [1191396](Iteracao2/UC4_EspecificarCT.md)             |    -                                                      |
| UC 5 (Requisitos + Design)  |  [1090552](Iteracao1/UC5_EspecificarColaborador.md)     |      [1090552](Iteracao1/UC5_EspecificarColaborador.md)   |    -                                                      |
| UC 6 (Requisitos + Design)  |  [1100241](Iteracao1/UC6_EspecificarTarefa.md)          |     [1100241](Iteracao1/UC6_EspecificarTarefa.md)         |    -                                                      |
| UC 7 (Requisitos + Design)  |   -                                                     |  [1191396](Iteracao2/UC7_RegistarFreelancer.md)           |    -                                                      |
| UC 8 (Requisitos + Design)  |   -                                                     |   [1100241](Iteracao2/UC8_PublicarTarefa.md)              | [1100241](Iteracao3/UC8_PublicarTarefa.md)                |
| UC 9 (Requisitos + Design)  |   -                                                     |     [1182115](Iteracao2/UC9_EfetuarCandidatura.md)        |  [1182115](Iteracao3/UC9/UC9_EfetuarCandidatura.md)           |
| UC 10 (Requisitos + Design) |   -                                                     |   [1090552](Iteracao2/UC10_SeriarAnuncio.md)              | [1090552](Iteracao3/UC10_SeriarAnuncio.md)                |
| UC 11 (Requisitos + Design) |    -     |      -                                                                                                   | [1182115](Iteracao3/UC11/UC11_AtualizarCandidatura.md)                      |
| UC 12 (Requisitos + Design) |    -     |      -                                                                                               | [1100241](Iteracao3/UC12/UC12_RetirarCandidatura.md)                        |
| UC 13 (Requisitos + Design) |    -     |      -                                                                                               | [1090552](Iteracao3/UC13/UC13_SeriarAutomaticamenteCandidaturasAnuncios.md) |
| UC 14 (Requisitos + Design) |    -     |      -                                                                                               | [1191396](Iteracao3/UC14/UC14_AdjudicarAnuncio.md)                          |

