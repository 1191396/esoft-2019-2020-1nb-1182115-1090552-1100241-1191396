# UC8 - Publicar tarefa

Os colaboradores das organizações podem publicar as tarefas por si	
anteriormente criadas. Da publicação de uma tarefa resulta um anúncio onde	
consta:
 (i) o período de publicitação da tarefa na plataforma;	 
 (ii) o	período de apresentação de candidaturas pelos freelancers;
 (iii) o período de seriação e decisão de atribuição da	tarefa a um	freelancer pela	organização;
 (iv) pelo tipo de regimento aplicável.	
Este último	estipula as	regras gerais pelas	quais se regem os processos	de candidatura, de seriação e de atribuição	de tarefa no âmbito	de	
um anúncio.	De momento devem ser suportados	os seguintes tipos:
 a) Seriação subjetiva com atribuição opcional: estipula que o processo de seriação dos candidatos assenta em critérios subjetivos definidos pela	
organização e que esta, no final, não está obrigada a atribuir a tarefa a nenhum dos candidatos;
 b) Seriação subjetiva com atribuição obrigatória: semelhante ao anterior, mas no qual a organização tem obrigatoriamente de atribuir a tarefa a um	dos	candidatos (desde que  xista pelo menos	um);
 c) Seriação e atribuição automática com base no segundo preço mais baixo: como	o próprio nome indica garante aos candidatos que o processo	de seriação e atribuição assenta  exclusivamente no preço apresentado pelos candidatos.	

## 1. Engenharia de Requisitos

### Formato Breve
O colaborador da organização dá início à publicação de uma tarefa. O sistema apresenta uma lista de tarefas e solicita a seleção da tarefa a ser publicada. O utilizador seleciona a tarefa a ser publicada. O sistema apresenta uma lista de tipos de regimento aplicável (tipos de seriação) e solicita ao colaborador a seleção do tipo de regimento. O colaborador seleciona o tipo de regimento. O sistema solicita os dados necessários ao colaborador ((i) o período de publicitação da tarefa na plataforma, (ii) o período de apresentação de candidaturas pelos freelancers, (iii) o período de seriação e decisão de atribuição da tarefa a um freelancer pela organização). O colaborador introduz os dados solicitados. O sistema valida e apresenta os dados ao colaborador, pedindo que os confirme. O colaborador confirma. O sistema regista os dados e informa o colaborador do sucesso da operação.


### SSD
![UC8_SSD.svg](UC8_SSD.svg)


### Formato Completo

#### Ator principal
Colaborador da Organização


#### Partes interessadas e seus interesses
* **Colaborador da Organização:** pretende que sejam publicadas as tarefas para que, posteriormente, os freelancers se possam candidatar.
* **Organização:** pretende que os seus colaboradores possam publicar as tarefas para que, posteriormente, os freelancers se possam candidatar.
* **Freelancer:** pretende conhecer as tarefas a que pode candidatar-se.
* **T4J:** pretende que a tarefa seja publicada para ser realizada para que, posteriormente, os freelancers se possam candidatar


#### Pré-condições
A tarefa tem que estar criada.


#### Pós-condições
A tarefa é publicada e dá origem a um anúncio. 


### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador da organização dá início à publicação de uma tarefa. 
2. O sistema apresenta uma lista de tarefas e solicita a seleção da tarefa a ser publicada. 
3. O utilizador seleciona a tarefa a ser publicada. 
4. O sistema apresenta uma lista de tipos de regimento aplicável e solicita ao colaborador a seleção do tipo de regimento. 
5. O colaborador seleciona o tipo de regimento. 
6. O sistema solicita os dados necessários ao colaborador.
7. O colaborador introduz os dados solicitados. 
8. O sistema valida e apresenta os dados ao colaborador, pedindo que os confirme.
9. O colaborador confirma. 
10. O sistema regista os dados e informa o colaborador do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O colaborador solicita o cancelamento da publicação da tarefa.

> O caso de uso termina.

3a. A tarefa pretendida não está definida no sistema.

> O colaborador informa o sistema de tal facto. O caso de uso termina.

5a. O tipo de regimento não está definido no sistema.

> O colaborador informa o sistema de tal facto. O caso de uso termina.
 
8a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 6)
>
	>	2a. O colaborador não altera os dados. O caso de uso termina.

8b. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 	1. O sistema alerta o colaborador para o facto. 
> 	2. O sistema permite a sua alteração (passo 6).
> 
	> 2a. O colaborador não altera os dados. O caso de uso termina. 

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
1. As datas referidas no caso de uso devem conter hora?	
\-


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC8_MD.svg](UC8_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  				  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador da organização dá início à publicação de uma tarefa.		| ... interage com o colaborador? | PublicarTarefaController 		|  PureFabrication 			|
|		| ... coordena o UC? 												| PublicarTarefaController 		|  Controller 				|
|		| ... fica responsável por criar instâncias de Anuncio? 			| RegistoAnuncio | PureFabrication |
|		| ... possui a classe RegistoAnuncio? | Plataforma 			| Low Coupling |
| 		| ... conhece o utilizador/gestor a usar o sistema?				| SessaoUtilizador	| Information Expert (IE): cf. documentação do componente de gestão de utilizadores.|
|		| ... sabe a que organização o utilizador/colaborador pertence?		| Plataforma	 | IE: conhece todas as organizações.|
| 		|																	| Organização	 | IE: conhece os seus colaboradores.|
| 		|																	| Colaborador	 | IE: conhece os seus dados (e.g. email).|
| 2. O sistema apresenta uma lista de tarefas e solicita a seleção da tarefa a ser publicada. | ... conhece as tarefas existentes a listar? | RegistoTarefa | IE: RegistoTarefa tem/agrega as tarefas daquela Organização. |
|		| ... que possui a classe RegistoTarefa? | Organizacao 				| Low Coupling | 
| 3. O colaborador seleciona a tarefa a ser publicada. | ... guarda a tarefa selecionada? | Anuncio | IE: Anuncio tem/agrega Tarefa |
| 4. O sistema apresenta uma lista de tipos de regimento aplicável e solicita ao colaborador a seleção do tipo de regimento. | ... conhece os tipos de regimento existentes a listar? | RegistoAnuncio | IE: RegistoAnuncio tem/agrega tipos de regimento |
| 5. O colaborador seleciona o tipo de regimento. | ... guarda o TipoRegimento selecionado? | Anuncio | IE: Anuncio tem/agrega TipoRegimento |
| 6. O sistema solicita os dados necessários ao colaborador. | ... guarda os dados introduzidos? |  Anuncio |	IE: Instância criada no passo 1 |
| 7. O colaborador introduz os dados solicitados. | | | |
| 8. O sistema valida e apresenta os dados ao colaborador, pedindo que os confirme.	| ... valida os dados do Anuncio (validação local) | Anuncio | IE: Possui os seu próprios dados.|
| 		| ... valida os dados do Anuncio (validação global) | RegistoAnuncio	| PureFabrication	|
| 9. O colaborador confirma. |	| 	| |
| 10. O sistema regista os dados e informa o colaborador do sucesso da operação. | ... guarda o Anuncio criado?	| RegistoAnuncio | PureFabrication |
| 		| ... notifica o colaborador? | PublicarTarefaUI  | |  


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organizacao
 * Tarefa
 * Colaborador
 * Anuncio
 * TipoRegimento


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * PublicarTarefaUI  
 * PublicarTarefaController
 * RegistoTarefa
 * RegistoAnuncio
 * RegistoColaborador


Outras classes de sistemas/componentes externos:

 * SessaoUtilizador


###	Diagrama de Sequência

![UC8_SD.svg](UC8_SD.svg)

 * Diagrama de Sequência Secundário - getEmailByLogin<br><br>
![IUC_getEmailByLogin](IUC_getEmailByLogin.svg)

 * Diagrama de Sequência Secundário - getOrganizacaoByEmailUtilizador<br><br>
![IUC_getOrganizacaoByEmailUtilizador](IUC_getOrganizacaoByEmailUtilizador.svg)

 * Diagrama de Sequência Secundário - getTarefasNaoPublicadasByEmail<br><br>
![IUC_getTarefasNaoPublicadasByEmail](IUC_getTarefasNaoPublicadasByEmail.svg)

 * Diagrama de Sequência Secundário - getTiposDeRegimento <br><br>
![IUC_getTiposDeRegimento](IUC_getTiposDeRegimento.svg)

###	Diagrama de Classes

![UC8_CD.svg](UC8_CD.svg)
