# Diagrama de Casos de Uso

![Diagrama de Casos de Uso](DUC.svg)

# Casos de Uso
| UC  | Descrição                                                               | It1   | It2   | It3   |                  
|:----|:------------------------------------------------------------------------|-------|-------|-------|
| UC1 | [Registar Organização](UC1_RegistarOrganizacao.md)                      |   C   |   R   |   -   |
| UC2 | [Definir Área de Atividade](UC2_DefinirArea.md)                         |   C   |   I   |   -   |
| UC3 | [Definir Categoria (de Tarefa)](UC3_DefinirCategoria.md)                |   C   |   R   |   -   |
| UC4 | [Especificar Competência Técnica](UC4_EspecificarCT.md)                 |   C   |   R   |   -   |
| UC5 | [Especificar Colaborador de Organização](UC5_EspecificarColaborador.md) |   C   |   R   |   -   |
| UC6 | [Especificar Tarefa](UC6_EspecificarTarefa.md)                          |   C   |   R   |   -   |
| UC7 | [Registar Freelancer](UC7_RegistarFreelancer.md)                        |   -   |   C   |   -   |
| UC8 | [Publicar Tarefa](UC8_PublicarTarefa.md)                                |   -   |   C   |   -   |
| UC9 | [Efetuar Candidatura](UC9_EfetuarCandidatura.md)                        |   -   |   C   |   -   |
| UC10 | [Seriar Anúncio](UC10_SeriarAnuncio.md)                                |   -   |   C   |   -   |

C - Criado <br/>
R - Refinado <br/>
I - Inalterado <br/>