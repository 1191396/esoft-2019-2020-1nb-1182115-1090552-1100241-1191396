# UC6 - Especificar tarefa

## 1. Engenharia de Requisitos

### Formato Breve
O colaborador da organização dá início à especificação de uma nova tarefa. O sistema solicita os dados necessários ao colaborador (uma designação, uma descrição informal e outra de carácter técnico, uma estimativa de duração e custo. O sistema apresenta uma lista de categorias de tarefa. O colaborador seleciona a categoria de tarefa em que se enquadra a tarefa. O sistema valida e apresenta os dados ao colaborador, pedindo que os confirme. O colaborador confirma. O sitema regista os dados e informa o colaborador do sucesso da operação.


### SSD
![UC6_SSD.svg](UC6_SSD.svg)


### Formato Completo

#### Ator principal
Colaborador da Organização

#### Partes interessadas e seus interesses
* **Colaborador da Organização:** pretende especificar as tarefas para que, posteriormente, estas sejam publicadas pelas organizações respetivas.
* **Organização:** pretende que os seus colaboradores possam especificar tarefas para posterior publicação.
* **Freelancer:** pretende conhecer as tarefas a que pode candidatar-se.
* **T4J:** pretende que a tarefa seja especificada para posterior publicação pela organização.


#### Pré-condições
A categoria de tarefa tem que estar criada


#### Pós-condições
A especificação da tarefa é registada no sistema. 


### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador dá início à especificação de uma nova tarefa. 
2. O sistema solicita os dados necessários ao colaborador.
3. O colaborador introduz os dados solicitados. 
4. O sistema apresenta uma lista de categorias de tarefa.
5. O colaborador seleciona a categoria de tarefa em que a tarefa se enquadra.
6. O sistema valida e apresenta os dados ao colaborador, pedindo que os confirme. 
7. O colaborador confirma. 
8. O sistema regista os dados e informa o colaborador do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O colaborador solicita o cancelamento da especificação da tarefa.

> O caso de uso termina.

5a. A categoria pretendida não está definida no sistema.

> O colaborador informa o sistema de tal facto. O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O colaborador não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o colaborador para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O colaborador não altera os dados. O caso de uso termina.

6c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 	1. O sistema alerta o colaborador para o facto. 
> 	2. O sistema permite a sua alteração (passo 3).
> 
	> 2a. O colaborador não altera os dados. O caso de uso termina. 

#### Requisitos especiais
1. A estimativa de duração de uma tarefa é indicada em dias.
2. Custo estimado de uma tarefa é indicado em POTs (moeda virtual interna à plataforma).

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
1. Existem outros dados que são necessários?
2. Todos os dados são obrigatórios?
3. Quando a categoria pretendida não existe, é necessário recolher alguma informação ou notificar alguém que isso aconteceu?
4. A lista de categorias pode ser extensa. Faz sentido permitir filtros (e.g. por área de atividade) e/ou pesquisa (e.g. pela descrição)?
5. Qual a frequência de ocorrência deste caso de uso?
	

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC6_MD.svg](UC6_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  				  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador dá início à especificação de uma nova tarefa. |	... interage com o colaborador? | DefinirTarefaUI |  PureFabrication |
|  |	... coordena o UC? | DefinirTarefaController |  Controller |
|  |	... fica responsável por criar instâncias de Tarefa? | RegistoTarefa | PureFabrication |
| 	|   ... possui a classe RegistoTarefa? | Organizacao 			| Low Coupling |
| 	|   ... conhece o utilizador/gestor a usar o sistema?	| SessaoUtilizador	| IE: cf. documentação do componente de gestão de utilizadores.|
|   |...sabe a que organização o utilizador/colaborador pertence?	|Plataforma	 |IE: conhece todas as organizações.|
| | Organização	| IE: conhece os seus colaboradores.|
| | Colaborador	| IE: conhece os seus dados (e.g. email).|
| 2. O sistema solicita os dados necessários ao colaborador. |		|     |		|
| 3. O colaborador introduz os dados solicitados. |	... guarda os dados introduzidos? | Tarefa | Information Expert (IE) - instância criada no passo 1 |
| 4. O sistema apresenta uma lista de categorias de tarefa. | ...conhece as categorias de tarefa existentes a listar? | RegistoCategoriaTarefa | PureFabrication |
| 	|   ... possui a classe RegistoCategoriaTarefa? | Plataforma 			| Low Coupling |
| 5. O  colaborador seleciona a categoria de tarefa em que a tarefa se enquadra. | ...guarda a categoria selecionada? | Tarefa | IE - Tarefa tem/agrega uma categoria de tarefa |
| 6. O sistema valida e apresenta os dados ao colaborador, pedindo que os confirme. |	...valida os dados da Tarefa (validação local) | Tarefa |IE - Possui os seu próprios dados.|
|	 |	...valida os dados da Tarefa (validação global) | RegistoTarefa | PureFabrication |
| 7. O colaborador confirma. | 		|     |		|
| 8. O sistema regista os dados e informa o colaborador do sucesso da operação. | ... guarda a Tarefa criada? | Organizacao  | IE - A Organização possui Tarefa |  
| | ... notifica o colaborador? | DefinirTarefaUI  | |  
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organizacao
 * Tarefa


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirTarefaUI  
 * DefinirTarefaController
 * RegistoTarefa
 * RegistoCategoriaTarefa


Outras classes de sistemas/componentes externos:

 * SessaoUtilizador


###	Diagrama de Sequência

![UC6_SD.svg](UC6_SD.svg)

 * Diagrama de Sequência Secundário - getEmailByLogin<br><br>
![IUC_getEmailByLogin](IUC_getEmailByLogin.svg)

 * Diagrama de Sequência Secundário - getOrganizacaoByEmailUtilizador<br><br>
![IUC_getOrganizacaoByEmailUtilizador](IUC_getOrganizacaoByEmailUtilizador.svg)
###	Diagrama de Classes

![UC6_CD.svg](UC6_CD.svg)
