# UC7 - Registar Freelancer

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia o registo de um freelancer na plataforma. O sistema solicita os dados necessários (i.e. nome, NIF, endereço postal, contacto telefónico, email, habilitações académicas, competências técnicas e graus de proficiência). O administrativo introduz os dados solicitados. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados do freelancer, gera a sua password, torna-o um utilizador registado e informa o administrativo do sucesso da operação.


### SSD
![UC7-SSD](UC7_SSD.svg)

### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses

* **Administrativo:** pretende que os colaboradores por ele registados especifiquem tarefas para a organização.
* **Freelancer:** pretende ser registado no sistema para poder aceder ás tarefas publicadas de maneira a se poderem propôr a realizar-las.



#### Pré-condições

-Existir competência técnica.
-Validação de candidatura pelo departamento de recursos humanos da TJ4.



#### Pós-condições

-A informação sobre o novo Freelancer é registada no sistema.


#### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a especificação de um novo freelancer.
1. 1 O sistema solicita os dados necessários (i.e. nome, NIF, endereço postal, contacto telefónico, email)
2. O administrativo introduz os dados solicitados.
2. 1 Solicita dados de habilitações académicas(grau, designação do curso, instituição, média do curso).
3. O Administrativo introduz dados solicitados.
3. 1 O sistema apresenta lista de competências técnicas
4. O administrativo seleciona uma competência técnica.
4. 1 Solicita outros dados(grau de proficiencia e data de reconhecimento).
5. O administrativo introduz os dados solicitados.
5. 1 O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.
6. O administrativo confirma os dados.
6. 1 O sistema regista os dados do freelancer, gera a sua password, torna-o um utilizador registado e informa o  administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

(alternativas globais ao fluxo principal ou especifica de um determiando passo)

*a. O administrativo solicita o cancelamento do registo de um novo freelancer.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 2)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 2)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o administrativo para o facto.
> 2. O sistema permite a sua alteração (passo 2).
>
	> 2a. O administrativo não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* Quais as regras de segurança aplicáveis à palavra-passe?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC7_MD.svg](UC7_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia o registo de um novo freelancer na plataforma.   		 |	... interage com o utilizador? | RegistarFreelancerUI    |  Pure Fabrication: não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio. |
|  		 |	... coordena o UC?	| RegistarFreelancerController | Controller    |
|  		 |	... cria instância de Freelancer?| RegistaFreelancer   | Creator (Regra1): no MD a Plataforma tem Freelancer e delega a RegistoFreelancer (HC + LC)   |
||...conhece o utilizador/administrativo a usar o sistema?|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
|||Plataforma|IE: conhece o seu Freelancer.|
|||Freelancer|IE: conhece os seus dados (e.g. email). |
| 1.1 O sistema solicita os dados necessários (i.e. nome, NIF, endereço postal, contacto telefónico, email).  		 |							 |             |                              |
| 2. O administrativo introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   Freelancer | Information Expert (IE) - instância criada no passo 1: possui os seus próprios dados.     |
| 2.1 Solicita dados de habilitações académicas(grau, designação do curso, instituição, média do curso). |							 |             |                              |
| 3. O Administrativo introduz dados solicitados. |	cria instância HabilitacoesAcademicas? | Freelancer | Creator (Regra1): no MD o Freelancer tem HabiliatacaoAcademica |
||...guarda os dados introduzidos? | HabilitacaoAcademica | (IE) - instância criada neste passo |
||...valida dados introduzidos? (Validacao Local) | Freelancer |IE: possui os próprios dados.|
| 3.1 O sistema apresenta lista de competências técnicas |	...conhece as Competencias Tecnicas?| RegistoCompetenciaTecnica  | no MD a Plataforma tem Freelancer e delega a RegistoCompetenciaTecnica (HC + LC)|
| 4. O administrativo seleciona uma competência técnica |							 |             |                              |
| 4.1 Solicita outros dados(grau de proficiencia e data de reconhecimento) |...conhece graus de proficiência?	|CompetenciaTecnica| IE: no MD CompetenciaTecnica tem/agrega GrauProficiencia |
| 5. O administrativo introduz dados solicitados | ...cria instância de ReconhecimentoTecnico? | Freelancer | Creator (Regra1) |
||...guarda os dados introduzidos?	 | ReconhecimentoTecnico     |IE: instancia criada neste passo.|
| 5.1 O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.   		 |	... valida os dados do Freelancer (validação local) | Freelancer |      IE: possui os seus próprios dados.|  	
|	 |	... valida os dados do Freelancer (validação global) | Plataforma  | IE: a Plataforma contém/agrega Freelancer.  |
| 6. O administrativo confirma os dados.   		 |							 |             |                              |
| 6.1 O sistema regista os dados do freelancer, gera a sua password, torna-o um utilizador registado e informa o administrativo do sucesso da operação.  		 |	... guarda o Freelancer criado? | RegistoFreelancer  | IE: No MD Plataforma contém/agrega Freelancer e delega a RegistoFreelancer (HC + LC).|
||... gera a password?|Plataforma|IE: concebido por terceiros.|
|| ... regista/guarda o Utilizador referente ao Freelancer? | AutorizacaoFacade  | IE: a gestão de utilizadores é responsabilidade do componente externo respetivo, cujo ponto de interação é através da classe "AutorizacaoFacade".|

### Sistematização ##

 Do racional resulta que as classes conceituais promovidas a classes de software são:

 * Administrativo
 * Plataforma
 * Freelancer
 * EnderecoPostal
 * HabilitacoesAcademicas
 * CompetenciaTecnica
 * GrauProficiencia
 * ReconhecimentoTecnico

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RegistarFreelancerUI  
 * RegistarFreelancerController
 * RegistoFreelancer



###	Diagrama de Sequência

![UC7_SD.svg](UC7_SD.svg)
![IUC7_RegistaFreelancerComoUtilizador.jpg](IUC7_RegistaFreelancerComoUtilizador.jpg)



###	Diagrama de Classes

![UC7_CD.svg](UC7_CD.svg)
