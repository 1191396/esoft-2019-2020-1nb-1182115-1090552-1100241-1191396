# Especificação suplementar

## Funcionalidades

1. Correio - Existência de serviços relacionados com envio/recepção de correio:
    > A gestão de utilizadores existente na T4J necessita de enviar dados para o email registado pelo utilizador.

2. Segurança - Acesso controlado a determinadas funcionalidades do sistema:
    > As interações dos utilizadores supramencionados devem ser precedidas de um processo de autenticação.
    > A utilização da plataforma por outras pessoas é restrita ao registo de organizações.

3. Workflow - Suporte para a gestão do estado de itens de trabalho:
    > Enquanto as tarefas não forem publicadas pelo colaborador, o acesso à tarefa é exclusivo aos colaboradores da organização respetiva.
    > Só após o registo de um freelancer (processo realizado pelos adminstrativos), este deve poder aceder à plataforma.
    > Findo o período de apresentação de candidaturas de um anúncio, é espoletado o processo de seriação dos candidatos em concordância com o tipo de regimento aplicável.

## Usabilidade
\-

## Fiabilidade/Confiabilidade
\-

## Desempenho
\-

## Suportabilidade

1. Testabilidade:
    > Devem ser especificados um conjunto relevante de testes de cobertura e mutação (e.g. unitários, funcionais e de integração) que assegure a qualidade do sistema desenvolvido.

2. Manutenibilidade:
    > Ao longo do tempo prevê-se a adoção de mais e variados tipos de regimento. Como tal, pretende-se que a adição de novos tipos de regimento esteja facilitada e, se possível, possa até	ser	realizada por terceiros. 

3. Configurabilidade:
    > A designação comercial da plataforma deve ser especificada por configuração aquando da sua implantação.
    > A interface gráfica da aplicação deve assentar numa paleta de cores estruturada em duas cores (primária e secundária) e esta ser configurável 
    aquando da sua implantação.

## + Restrições

### Restrições de design

 > Reutilizar o componente de gestão de utilizadores existente na T4J baseado em identificador de utilizador (i.e. email) e palavra- passe (cf. documentação). 
 > Adotar boas práticas de identificação de requisitos e de análise e design de software OO.
 > Devem ser adotados os princípios GRASP (GENERAL RESPONSIBILITY ASSIGNMENT SOFTWARE PATTERNS):
    > Protected Variation aplicada aos algoritimos externos. 
    > Modularidade - Aplicação dos padrões Low Coupling e High Cohesion.
    > Polimorfismo - Preferência à adoção de Interfaces em vez de Superclasses.

### Restrições de implementação

 > Implementar o núcleo principal do software em Java.
 > Adotar normas de codificação reconhecidas.


### Restrições de interface

> Em termos visuais, a interface gráfica da aplicação deve assentar numa paleta de cores estruturada em duas cores (primária e secundária) e esta ser configurável 
aquando da sua implantação. 


### Restrições físicas
\-