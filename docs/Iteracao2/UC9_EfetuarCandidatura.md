# UC9 - Efetuar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer inicia a candidatura a um anúncio. O Sistema retorna uma lista de anúncios ao qual o freelancer é elegível. Este seleciona um único anuncio. De seguida, a plataforma pede-lhe dados de preenchimento obrigatório tais como o valor pretendido pela realização da tarefa e o número de dias necessários à sua realização. O freelancer introduz a informação pretendida. De seguida, a plataforma solicita dados opcionais (texto de apresentação e/ou motivação) ao qual o freelancer poderá ou nao introduzir. à posterior, o sistema apresenta os dados e solicita a confirmação do freelancer. O freelancer confirma e a plataforma retorna a informação da operação bem sucedida

### SSD
![UC9_SSD.svg](UC9_SSD.svg)

### Formato Completo

#### Ator principal
Freelancer

#### Partes interessadas e seus interesses
* **Freelancer:** Pretende candidatar-se aos anuncios disponíveis na plataforma que melhor se adaptem às suas capacidades.
* **T4J:** Pretende que os anuncios sejam entregues aos profissionais que melhor se qualifiquem para a realização da tarefa pretendida.

#### Pré-condições
* Registo de Freelancers
* Anuncios inseridos

#### Pós-condições
\-

### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer inicia a candidatura a um anúncio. 
2. O Sistema retorna uma lista de anúncios ao qual o freelancer é elegível. 
3. O freelancer seleciona um único anuncio. 
4. A plataforma pede-lhe dados de preenchimento obrigatório (valor pretendido e o número de dias necessários à sua realização) assim como os dados não obrigatórios (Texto de apresentação e/ou motivação).
5. O freelancer introduz os dados obrigatórios e **(opcional)** dados opcionais. 
6. A plataforma apresenta os dados e solicita a confirmação. 
7. O freelancer confirma.
8. A plataforma retorna a informação da operação bem sucedida

#### Extensões (ou fluxos alternativos)

*a. O Freelancer solicita o cancelamento da candidatura.

> O caso de uso termina.

2a. O sistema deteta que a lista de anúncios está vazia.
>	1. O sistema informa o freelancer de tal facto.
>
	> O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
> 1. O sistema informa quais os dados em falta. 
> 2. O sistema permite a introdução dos dados em falta (passo 4).
> 
	> 2a. O freelancer não altera os dados. O caso de uso termina. 

6b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o freelancer para o facto. 
> 2. O sistema permite a sua alteração (passo 4 e passo 6).
> 
	> 2a. O freelancer não altera os dados. O caso de uso termina. 
 
#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC
![UC9_MD.svg](UC9_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O freelancer inicia a candidatura a um anúncio disponível.   		 |	... interage com o freelancer? | CandidatarAnuncioUI    | PureFabrication |
|  		 |	... coordena o UC?	| CandidatarAnuncioController | Controller    |
|  		 |	... cria instância de CandidaturaAnuncio| rca : CriarCandidatarAnuncio   | Padrão HC/LC + Creator |
|  		 |	... gera o identificador único| rca : CriarCandidatarAnuncio   | IE  |
| 2. O sistema e apresenta uma lista de anuncios disponíveis. | ...quem tem conhecimento das Anúncios Disponíveis? | Plataforma | IE |
| 3. O Freelancer seleciona um único anúncio. ||||||
| 4. O sistema solicita os dados obrigatórios (valor pretendido e o número de dias necessários à sua realização) assim como os dados não obrigatórios (Texto de apresentação e/ou motivação)  		 ||||
| 5. O freelancer introduz os dados solicitados e **(opcional)** dados opcionais.  		 |	...Guarda os dados introduzidos  |rca : CriarCandidatarAnuncio |IE |
| 6. A plataforma valida e apresenta os dados da candidatura do anúncio, pedindo que os confirme. |	...valida os dados da  (validação local) | CandidatarAnuncio |IE |
|	 |	...valida os dados da Categoria (validação global) | rca : CriarCandidatarAnuncio | IE |
| 7. O freelancer confirma. ||||||
| 8. A Plataforma regista os dados e informa o freelancer do sucesso da operação.|	... guarda a Candidatura de anúncio criada? | rca : CriarCandidatarAnuncio | IE|  
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:
* Plataforma
* CriarCandidatarAnuncio
* CandidaturaAnuncio
* CategoriaTarefa
* Anuncio
* freelancer

Outras classes de software (i.e. Pure Fabrication) identificadas:
* CandidaturaAnuncioUI
* CandidaturaAnuncioController

###	Diagrama de Sequência

![UC9_SD.svg](UC9_SD.svg)

###	Diagrama de Classes
![UC9_CD.svg](UC9_CD.svg)




