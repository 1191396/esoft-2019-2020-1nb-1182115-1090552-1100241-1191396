﻿# UC10 - Seriar Anúncio

## 1. Engenharia de Requisitos

### Formato Breve
Colaborador dá início a novo processo de seriação. Sistema apresenta os anúncios elegivéis para o colaborador. O Colaborador escolhe um anúncio. Sistema apresenta uma lista de colaboradores para adicionar a processo (Opcional). Colaborador seleciona novo colaborador ao processo (opcional). Sistema fornece lista de candidaturas para esse anúncio e solicita o grau de classificação. Colaborador introduz grau de classificação. Sistema apresenta lista ordenada e pede para confirmar os dados. Colaborador confirma. Sistema informa colaborador do sucesso da operação.

### SSD
![UC10_SSD.svg](UC10_SSD.svg)


### Formato Completo

#### Ator principal
Colaborador da Organização


#### Partes interessadas e seus interesses
* **Colaborador da Organização:** Pretende que sejam seriados os anuncios, para a organização poder proceder á atribuição de tarefas a Freelancers 
* **Organização:** Pretende obter a lista ordenada de candidaturas, por classificação, para posteriormente proceder á atribuição da tarefa a um freelancer.
* **Freelancer:** pretende saber se foi selecionado para a tarefa a que o anúncio diz respeito.
* **T4J:** Pretende que sejam seriados os anuncios, para que, posteriormente, sejam atribuidas tarefas a Freelancers. 


#### Pré-condições
O anúncio tem de estar criado e ser elegível.
A lista de candidaturas elegíveis para esse anúncio tem de estar cridada.

#### Pós-condições
As candidaturas são ordenadas e dará início ao processo de atribuição da tarefa a um freelancer. 


### Cenário de sucesso principal (ou fluxo básico)

1. Colaborador dá início a novo processo de seriação. 
2. Sistema apresenta os anúncios elegivéis para o colaborador. 
3. O Colaborador escolhe um anúncio. 
4. Sistema apresenta uma lista de colaboradores para adicionar a processo (Opcional). 
5. Colaborador seleciona novo colaborador ao processo (opcional). 
6. **Os passos 4 a 5 rerador pretender adicionar mais colaboradores.**
7. Sistema fornece lista de candidaturas para esse anúncio e solicita o grau de classificação.  
8. Colaborador introduz grau de classificação. 
9. **Os passos 7 a 8 repetem-se enquanto houverem candidaturas para serem classificadas.**
10. Sistema apresenta lista ordenada e pede para confirmar os dados. 
11. Colaborador confirma. 
12. Sistema informa colaborador do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O colaborador solicita o cancelamento do processo de seriação.

> O caso de uso termina.

2a. O sistema deteta que a lista de anúncios está vazia.
>	1. O sistema informa o colaborador de tal facto.
>
	> O caso de uso termina.

5a. O e-mail do colaborador adicional não é válido.

>	1. O sistema informa o Colaborador.
>	2. O colaborador introduz o e-mail corrigido (passo 5). 

7a. O sistema deteta que a lista de candidaturas está vazia.
>	1. O sistema informa o colaborador de tal facto.
>
	> O caso de uso termina. 

10a. Dados mínimos obrigatórios em falta. 
> 	1. O sistema informa quais os dados em falta. 
>	2. O sistema permite a introdução dos dados em falta (passo 8). 
> 
	> 2a. O Colaborador não altera os dados. O caso de uso termina. 

10b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
>  	1. O sistema alerta o colaborador para o facto.
>	2. O sistema permite a sua alteração (passo 8).
>
	> 2a. O Colaborador não altera os dados. O caso de uso termina. 

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
\-
* Qual a frequência de ocorrência deste caso de uso?
* No caso dos processos de seriação não automáticos, deverá ser gerado um alerta para o colaborador realizar o processo em questão?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10_MD.svg](UC10_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  				  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. Colaborador dá início a novo processo de seriação.  	|	... interage com o colaborador? | SeriarAnuncioUI |  PureFabrication |
|  																	|	... coordena o UC? | SeriarAnuncioController |  Controller |
|  																	|	... fica responsável por criar instâncias de Processos de Seriacao? | RegistoProcessoSeriacao | PureFabrication |
| 															| ...possui a classe RegistoProcessoSeriacao? | Organizacao | Low Coupling |
| 																	|   ...conhece o utilizador/gestor a usar o sistema?	| SessaoUtilizador	| IE: cf. documentação do componente de gestão de utilizadores.|
|   																|...sabe a que organização o utilizador/colaborador pertence?	|Plataforma	 |IE: conhece todas as organizações.|
| 															|		| 								Organização	| IE: conhece os seus colaboradores.|
| | | Colaborador	| IE: conhece os seus dados (e.g. email).|
| 2. Sistema apresenta os anúncios elegivéis para o colaborador. |	...conhece os anúncios existentes a listar?	| RegistoAnuncio     |	IE: RegistoAnuncio tem/agrega os anuncios daquela Plataforma	|
|															|...que possui a classe RegistoAnuncio? | Plataforma | Low Coupling|
| 3. O Colaborador escolhe um anúncio.  |	... guarda o Anuncio seleccioando? | Plataforma | IE: Plataforma tem/agrega Anuncio |
| 4. Sistema apresenta uma lista de colaboradores para adicionar a processo **(Opcional)**.  | ...conhece os colaboradores existentes a listar? | RegistoColaboradores | IE: RegistoColaborador tem/agrega Colaboradores|
| 5. Colaborador seleciona novo colaborador ao processo **(Opcional)**. | ...guarda o Colaborador selecionado? | Colaborador |  |
| 6. Sistema fornece lista de candidaturas para esse anúncio e solicita o grau de classificação. |	...conhece as candidaturas existentes a listar?  | RegistoCandidatura |IE - RegistoCandidatura tem/agraga candidaturas.|
|    |  ...que possui a classe RegistoCandidatura | Anuncio | Low Coupling|
| 7. Colaborador introduz grau de classificação. | ...guarda o Grau de Classificação?		| Classificacao     |IE: Classificacao tem/agrega Grau de Classificacao		|
| 8.  Sistema apresenta lista ordenada e pede para confirmar os dados.   | ...valida os dados do ProcessoSeriacao (validação local) |ProcessoSeriacao | IE: Possui os seus próprios dados.|
|    |  ...valida os dados do ProcessoSeriacao (validação global) | RegistoProcessoSeriacao | Pure Fabrication|
| 9. Colaborador confirma. | | | |
| 10. Sistema informa colaborador do sucesso da operação. | ... guarda o ProcessoSeriacao criado? | RegistoProcessoSeriacao  | IE - Pure Fabrication |  
| | ... notifica o colaborador? | SeriarAnuncioUI  | |  
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organizacao
 * Classificacao
 * Colaborador
 * Anuncio
 * TipoRegimento
 * ProcessoDeSeriacao
 * Candidatura


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarAnuncioUI  
 * SeriarAnuncioController
 * RegistoAnuncio
 * RegistoCandidatura
 * RegistoColaborador
 * RegistoTarefa
 * RegistoProcessoSeriacao
 * RegistoClassificacao


Outras classes de sistemas/componentes externos:

 * SessaoUtilizador
 * AplicacaoPOT


###	Diagrama de Sequência

![UC10_SD.svg](UC10_SD.svg)

 * Diagrama de Sequência Secundário - getEmailByLogin<br><br>
![IUC_getEmailByLogin](IUC_getEmailByLogin.svg)

 * Diagrama de Sequência Secundário - IUC_getOrganizacaoByEmailUtilizador<br><br>
![IUC_getOrganizacaoByEmailUtilizador](IUC_getOrganizacaoByEmailUtilizador.svg)

 * Diagrama de Sequência Secundário - IUC_getAnuncioNaoSeriadoByEmail<br><br>
![IUC_getAnuncioNaoSeriadoByEmail](IUC_getAnuncioNaoSeriadoByEmail.svg)	

 * Diagrama de Sequência Secundário - IUC_getTarefasPublicadasByEmail<br><br>
![IUC_getTarefasPublicadasByEmail](IUC_getTarefasPublicadasByEmail.svg)

 * Diagrama de Sequência Secundário - IUC_getListaCandidaturaOrdenada<br><br>
![IUC_getListaCandidaturaOrdenada](IUC_getListaCandidaturaOrdenada.svg)


###	Diagrama de Classes

![UC10_CD.svg](UC10_CD.svg)
