# Análise OO #

## Racional para identificação de classes de domínio ##

### _Lista de Categorias_ ###

**Transações (do negócio)**

*

---

**Linhas de transações**

*

---

**Produtos ou serviços relacionados com transações**

*Tarefa

---


**Registos (de transações)**

*  

---  


**Papéis das pessoas**

* Administrativo
* Freelancer
* Colaborador (de Organização)
* Gestor (de Organizacao)
* Utilizador
* Utilizador Não Registado

---


**Lugares**

* Endereço Postal
* Plataforma

---

**Eventos**

* 

---


**Objectos físicos**

*

---


**Especificações e descrições**

* Área de Atividade
* Competência Técnica
* Categoria de tarefa
* Tarefa
* Habilitação Académica
* Tipo de Regimento
* Anúncio
* Classificação
* Processo Seriação
* Candidatura
* Reconhecimento Tecnico
---


**Catálogos**

*  

---


**Conjuntos**

* Lista de competências técnicas requeridas 
* Lista de competências técnicas do Freelancer

---


**Elementos de Conjuntos**

* Caracter de competência técnica requerida 

---


**Organizações**

*  T4J (Plataforma)
*  Organização

---

**Outros sistemas (externos)**

*  (Componente Gestão Utilizadores)
*  AlgoritmoGeradorDePasswords

---


**Registos (financeiros), de trabalho, contractos, documentos legais**

* 

---


**Instrumentos financeiros**

*  

---


**Documentos referidos/para executar as tarefas/**

* 
---



###**Racional sobre identificação de associações entre classes**###

| Conceito (A) 		        |  Associação   		|  Conceito (B) |
|----------	   		        |:-------------:		|------:       |
| Plataforma			    | tem registadas        | Organização  |
|						    | tem/usa     			| Freelancer  |
|						    | tem     			    | Administrativo  |
| 						    | possui     			| Competência Técnica  |
| 						    | possui     			| Área de Atividade  |
| 						    | possui     			| Categoria de Tarefa  |
|                           | possui                | Tarefa |
| 						    | possui     			| Anúncio  |
| Organizacao			    | localizada em         | Endereço Postal  |
|						    | tem     	            | Gestor de Organização |
|						    | possui		     	| Colaborador |    
| 						    | possui     			| Tarefa  |
| Administrativo  	        | define    		 	| Área de Atividade  |
|   					    | especifica            | Competência Técnica  |
|   					    | define                | Categoria de Tarefa  |
|                           | trabalha para         | Plataforma |
|						    | atua como			    | Utilizador |
|						    | regista               | Freelancer |
| Gestor (de Organização)   | atua como             | Colaborador |
| Competência Técnica       | referente a           | Área de Atividade  |
|                           | Aplica                | Grau Proficiência|  
| Freelancer			    | atua como			    | Utilizador |
| 			                | candidata-se a        | Anúncio |
| 			                | realiza               | Tarefa |
|                           | tem                   | Habilitação Académica |
|                           | tem                   | Lista de Competências Técnicas |
|                           | tem                   | Reconhecimento Técnico |
|                           | tem                   | Endereço Postal |
| Colaborador			    | atua como			    | Utilizador |
|               		    | especifica    		| Tarefa     |
|               		    | publica    		    | Tarefa     |
|               		    | seria    		        | Anúncio    |
| Tarefa                    | possui    		    | Categoria de Tarefa      |
|                           | resulta em   		    | Anúncio      |
| Categoria de Tarefa	    | possui    		    | Competência Técnica     |
|                   	    | possui    		    | Área Atividade    |
|                   	    | tem    		        | Lista de Competências Técnicas requeridas    |
| Anuncio                   | guia-se por           | Tipo Refimento|
|                           | inicia                | Processo Seriação|
|                           | recebe                | Candidatura |
| Reconhecimento Tecnico    | aplica                | Grau de Proficiencia|
|                           | referente a           | Competência Tecnica|
| Processo Seriacao         | segundo as regra estabelecidas por | Tipo Regimento|
|                           | dá lugar a            | Classificação|
| Lista de Competências Técnicas requeridas | tem   | Caracter de Competência Técnica requerida |
| Lista de Competências Técnicas do Freelancer | tem | Data de Validação pelo Administrativo |
| Caracter de Competência Técnica | referente a | Competência Técnica |
| Data de Validação          | referente a | Competência Técnica |


## Modelo de Domínio

![MD.svg](MD_Global.svg)



