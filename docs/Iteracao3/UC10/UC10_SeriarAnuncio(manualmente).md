﻿# UC10 - Seriar (manualmente) Candidaturas de Anúncio

## 1. Engenharia de Requisitos

### Formato Breve
Colaborador dá início a novo processo de seriação, não automático, de um anúncio por si publicado. Sistema apresenta os anúncios elegivéis para o colaborador (em fase de seriação não automática e que ainda não foram seariados) e pede para escolher um. O Colaborador escolhe um anúncio. Sistema fornece lista de candidaturas para esse anúncio, e que ainda estejam por classificar, e solicita a sua classificação e **justificação da mesma**. Colaborador introduz a sua classificação. Sistema apresenta uma lista de colaboradores da organização e pede para seleccionar os outros participantes no processo (Opcional). Colaborador seleciona novo colaborador ao processo (opcional). **O sistema solicita o texto de conclusão do processo de seriação. O colaborador introduz o texto de conclusão.** O sistema valida e apresenta os dados, pedindo que os confirme. O colaborador confirma. **O sistema verifica se o tipo de seriação é com atribuição obrigatória e prossegue com o processo de atribuição.** O sistema regista os dados juntamente com a data/hora atual e informa o colaborador do sucesso da operação. 

### SSD
![UC10_SSD.svg](UC10_SSD.svg)


### Formato Completo

#### Ator principal
Colaborador da Organização


#### Partes interessadas e seus interesses
* **Colaborador da Organização:** pretende seriar as candidaturas que um anúncio recebeu.
* **Organização:** pretende contratar pessoas externas (outsourcing) para a realização de determinadas tarefas e com competências técnicas apropriadas.
* **Freelancer:** pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* **T4J:** pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa. 


#### Pré-condições
O anúncio tem de estar criado e ser elegível.
A lista de candidaturas elegíveis para esse anúncio tem de estar cridada.

#### Pós-condições
A informação do processo de seriação é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. Colaborador dá início a novo processo de seriação, não automático, de um anúncio por si publicado.  
2. Sistema apresenta os anúncios elegivéis para o colaborador (em fase de seriação não automática e que ainda não foram seariados) e pede para escolher um.  
3. O Colaborador escolhe um anúncio. 
4. O sistema mostra as candidaturas que o anúncio selecionado recebeu e que ainda estejam por classificar e solicita a escolha de uma dessas candidaturas.
5. O colaborador seleciona uma candidatura.
6. O sistema solicita a classificação da candidatura selecionada.
7. **O colaborador indica a classificação e justificação da mesma.**
8. Os passos 4 a 7 repetem-se até que estejam classificadas todas as candidaturas.
9. O sistema mostra a lista dos outros colaboradores da mesma organização (não selecionado) e solicita a seleção de um colaborador participante no processo de seriação.
10. O colaborador seleciona um colaborador (opcional).
11. Os passos 9 e 10 repetem-se até que estejam selecionados todos os outros colaboradores participantes no processo de seriação.
12. **O sistema solicita o texto de conclusão do processo de seriação.**
13. **O colaborador apresenta o texto de conclusão do processo.**
14. Valida e apresenta os dados, pedindo ao colaborador para confirmar.
15. O colaborador confirma.
16. **O sistema verifica se o tipo de seriação é com atribuição obrigatória e prossegue com o processo de atribuição.**
17. O sistema regista os dados juntamente com a data/hora atual e informa o colaborador do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O colaborador solicita o cancelamento do processo de seriação.

> O caso de uso termina.

2a. O sistema deteta que a lista de anúncios está vazia.
>	1. O sistema informa o colaborador de tal facto.
>
	> O caso de uso termina.

5a. O e-mail do colaborador adicional não é válido.

>	1. O sistema informa o Colaborador.
>	2. O colaborador introduz o e-mail corrigido (passo 5). 

7a. O sistema deteta que a lista de candidaturas está vazia.
>	1. O sistema informa o colaborador de tal facto.
>
	> O caso de uso termina. 

12a. Dados mínimos obrigatórios em falta. 
> 	1. O sistema informa quais os dados em falta. 
>	2. O sistema permite a introdução dos dados em falta (passo 2). 
> 
	> 2a. O Colaborador não altera os dados. O caso de uso termina. 

**16a. Tipo de seriação é com atribuição opcional.**
> 	1. O sistema pausa o processo e este fica á espera para ser continuado pelo Gestor de Organização (UC14). 
> 
	> O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
\-
* Qual a frequência de ocorrência deste caso de uso?
* O mesmo lugar/classificação pode ser atribuído a mais do que uma candidatura (e.g. em caso de empate)?
* O processo de seriação pode ser concluído havendo candidaturas por classificar?
* Há algum motivo que possa levar à desclassificação de uma candidatura?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10_MD.svg](UC10_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  				  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. Colaborador dá início a novo processo de seriação.  	|	... interage com o colaborador? | SeriarAnuncioUI |  PureFabrication |
|  																	|	... coordena o UC? | SeriarAnuncioController |  Controller |
| 																	|   ...conhece o utilizador/gestor a usar o sistema?	| SessaoUtilizador	| IE: cf. documentação do componente de gestão de utilizadores.|
|   																|...sabe a que organização o utilizador/colaborador pertence?	
| | | RegistoOrganizacoes| IE: no MD Plataforma conhece organizações e delega a RegistoOrganizações, por aplicação HC+LC|
| 															|		| 								Organização	| IE: conhece os seus colaboradores.|
| | | Colaborador	| IE: conhece os seus dados (e.g. email).|
| |...conhece o RegistoOrganizacoes? |Plataforma	 |IE: Plataforma tem RegistoOrganizacoes|
| 2. Sistema apresenta os anúncios elegivéis para o colaborador. | ...conhece os anúncios existentes a listar?	 | RegistoAnuncio | IE: no MD a Plataforma possui Anúncios. Por aplicação LC+HC delega a RegistoAnuncios |
||...conhece o RegistoAnuncios| Plataforma |IE: no MD Plataforma conhece organizações e delega a RegistoAnuncios, por aplicação HC+LC|| 
| 3. O Colaborador escolhe um anúncio.  | | | |
| 4.  Sistema fornece lista de candidaturas para esse anúncio e solicita a escolha de uma candidatura. |...conhece as candidaturas existentes a listar? |ListaCandidaturas| IE: no MD o Anuncio possui Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas|
||...conhece ListaCandidaturas| Anúncio| IE: no MD o Anuncio possui Candidaturas.|
| 5. O colaborador seleciona uma candidatura.||||
| 6. O sistema solicita a classificação da candidatura selecionada. ||||
| 7. O colaborador indica a classificação e justificação da mesma.|...cria a instância Classificacao?| ProcessoSeriacao| Creator: Processo Seriacao agrega instâncias de Classificação |
||...possui ProcessoSeriacao?|Anuncio||
||...guarda os dados intrudozidos?| Classificacao| IE: Classificacao guarda os seus próprios dados|
||...valida os dados introduzidos?| ProcessoSeriacao|IE: ProcessoSeriacao agrega instâncias de Classificação|
| 8. Sistema apresenta uma lista de colaboradores para adicionar a processo|..conhece os colaboradores existentes a listar? | ListaColaboradores | IE: no MD a Organizacao possui Colaboradores. Por aplicação de HC+LC delega a ListaColaboradores|
||...conhece ListaColaboradores|Organizacao| IE: no MD a Organizacao possui Colaboradores|
| 9.  O colaborador seleciona um colaborador.|...guarda novo colaborador participantes?|ProcessoSeriacao||
| 10. O sistema solicita o texto de conclusão do processo de seriação. ||||
| 11. O colaborador apresenta o texto de conclusão do processo.|...guarda o texto de conclusão?|ProcessoSeriacao||
| 12. Valida e apresenta os dados, pedindo ao colaborador para confirmar.||||
| 13. O colaborador confirma. |...guarda os dados do ProcessoSeriacao?|Anuncio||
| 15. O sistema verifica se o tipo de seriação é com atribuição obrigatória e, caso seja, prossegue com o processo de atribuição.|...conhece os requisitos da atribuição?|TipoRegimento|IE: TipoRegimento tem as regras de ordenação de candidatura | 
||...possui TipoRegimento|ProcessoSeriacao|IE: ProcessoSeriacao tem tipoRegimento|  
||...responsável pela adjudicacao da tarefa?| Plataforma| IE: Plataforma responsável pela atribuição automática| 
| 16. O sistema regista os dados juntamente com a data/hora atual e informa o colaborador do sucesso da operação.|...valida o ProcessoSeriacao? (validação local)|ProcessoSeriacao| IE: O processo de sericação tem os seus próprios dados||||
||...valida o ProcessoSeriacao? (validação global)|Anuncio|IE: o Anuncio contém Processos de Seriação|
||...informa o sucesso de operação?|SeriarAnuncioUI|IE:|

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organizacao
 * Classificacao
 * Colaborador
 * Anuncio
 * TipoRegimento
 * ProcessoSeriacao
 * Candidatura
 * Freelancer
 * Tarefa


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarAnuncioUI  
 * SeriarAnuncioController
 * RegistoAnuncio
 * RegistoOrganizacao
 * ListaCandidaturas
 * ListaColaboradores
 * ListaTarefas

###	Diagrama de Sequência

![UC10_SD.svg](UC10_SD.svg)

 * Diagrama de Sequência Secundário - getEmailByLogin<br><br>
![IUC_getEmailByLogin](IUC_getEmailByLogin.svg)

 * Diagrama de Sequência Secundário - IUC_getOrganizacaoByEmailUtilizador<br><br>
![IUC_getOrganizacaoByEmailUtilizador](IUC_getOrganizacaoByEmailUtilizador.svg)

 * Diagrama de Sequência Secundário - IUC_adjudicacaoDeTarefa<br><br>
![IUC_adjudicacaoDeTarefa](IUC_adjudicacaoDeTarefa.svg)


###	Diagrama de Classes

![UC10_CD.svg](UC10_CD.svg)
