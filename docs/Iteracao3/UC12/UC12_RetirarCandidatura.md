# UC12 - Retirar Candidatura Submetida

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer inicia a retirada de uma candidatura a um anúncio. O sistema apresenta **as candidaturas realizadas por ele aos anúncios que se encontram em período válido de apresentação de candidaturas**. O freelancer seleciona a candidatura a retirar. O sistema apresenta a candidatura selecionada, pedindo que confirme a sua eliminação. O freelancer confirma. O sistema retira a candidatura a operação e informa o freelancer do sucesso da operação.

### SSD
![UC12_SSD.svg](UC12_SSD.svg)


### Formato Completo

#### Ator principal

Freelancer

#### Partes interessadas e seus interesses
* **Freelancer:** pretende remover a candidatura previamente realizada a um anúncio.
* **Organizacão:** pretende que os candidatos que se propuseram à realização da tarefa se mantenham disponíveis após atribuição da mesma.
* **T4J:** pretende manter a lista de candidaturas atualizadas.


#### Pré-condições
O freelancer tem que ter realizado uma candidatura a um anúncio para poder removê-la.

#### Pós-condições
A candidatura é removida do sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer inicia a retirada de uma candidatura a um anúncio. 
2. O sistema apresenta **as candidaturas realizadas por ele aos anúncios que se encontram em período válido de apresentação de candidaturas**. 
3. O freelancer seleciona a candidatura a retirar. 
4. O sistema apresenta a candidatura selecionada, pedindo que confirme a sua eliminação. 
5. O freelancer confirma.
6. O sistema retira a candidatura a operação e informa o freelancer do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O freelancer solicita o cancelamento da remoção da candidatura.

> O caso de uso termina.

4a. O freelancer não seleciona nenhuma candidatura.
>	1. O sistema informa que nenhuma candidatura foi selecionada.
>	2. O sistema permite a seleção da candidatura (passo 3).
>
	>	2a. O freelancer não seleciona. O caso de uso termina.
	
6a. A candidatura selecionada já não está disponível para eliminação (passou o período de apresentação de candidaturas durante o processo).
>	1. O sistema informa o freelancer.
>	2. O caso de uso termina.



#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
\-


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC12_MD.svg](UC12_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O freelancer inicia a retirada de uma candidatura a um anúncio. |... interage com o utilizador?	|RetirarCandidaturaUI			|Pure Fabrication|
|  																	|	... coordena o UC?			| RetirarCandidaturaController 	| Controller    |
| 																	|... conhece o utilizador a usar o sistema?|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|	
| 																	|... conhece todos freelancers?|RegistoFreelancers|IE: no MD a Plataforma possui Freelancer. Por aplicação de HC+LC delega a RegistaFreelancer|	
|																	|... conhece todos os anúncios no sistema? | RegistoAnuncios | IE: no MD a Plataforma possui Anuncio. Por aplicação de HC+LC delega a RegistaAnuncio |
|																	|... conhece as candidaturas? | Anuncio | IE: Cada Anuncio tem uma ListaCandidaturas |
| 2. O sistema apresenta **as candidaturas realizadas por ele aos anúncios que se encontram em período válido de apresentação de candidaturas**. |... conhece o período válido de apresentação? | Anuncio | IE: Anuncio possui todos os seus períodos |
| 3. O freelancer seleciona a candidatura a retirar. 											||||										
| 4. O sistema apresenta a candidatura selecionada, pedindo que confirme a sua eliminação. 		||||																				
| 5. O freelancer confirma. 																	|... é responsável por retirar a candidatura do sistema?|ListaCandidaturas|IE: Anuncio possui ListaCandidaturas. |	
| 6. O sistema retira a candidatura a operação e informa o freelancer do sucesso da operação.	|...informa o colaborador?|PublicarTarefaUI| |
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organizacao
 * Candidatura


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RetirarCandidaturaUI  
 * RetirarCandidaturaController
 * RegistoFreelancers
 * RegistoAnuncios
 * ListaCandidaturas

 Outras classes de sistemas/componentes externos:

 * SessaoUtilizador

###	Diagrama de Sequência

![UC12_SD.svg](UC12_SD.svg)

* Diagrama de Sequência Secundário - IUC_getEmailByLogin<br>
![IUC_getEmailByLogin](IUC_getEmailByLogin.svg)

 * Diagrama de Sequência Secundário - IUC_getListCandidaturasByFreelancer <br>
![IUC_getListCandidaturasByFreelancer](IUC_getListCandidaturasByFreelancer.svg)

 * Diagrama de Sequência Secundário - IUC_retiraCandidatura<br>
![IUC_retiraCandidatura](IUC_retiraCandidatura.svg)

###	Diagrama de Classes

![UC12_CD.svg](UC12_CD.svg)

