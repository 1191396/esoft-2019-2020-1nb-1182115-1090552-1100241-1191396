# Análise OO #
O processo de construção do modelo de domínio é baseado nos casos de uso, em especial os substantivos utilizados, e na descrição do enunciado.
## Racional para identificação de classes de domínio ##
Para a identificação de classes de domínio usa-se a lista de categorias das aulas TP (sugeridas no livro). Como resultado temos a seguinte tabela de conceitos (ou classes, mas não de software) por categoria.

### _Lista de Categorias_ ###

**Transações (do negócio)**

* Anúncio
* Processo de Seriação
* Candidatura
* __Adjudicacao__

---

**Linhas de transações**

* Classificacao
---

**Produtos ou serviços relacionados com transações**

*  Tarefa

---


**Registos (de transações)**

*

---  


**Papéis das pessoas**

* Administrativo
* Freelancer
* Colaborador (de Organização)
* Gestor (de Organização)
* Utilizador
* Utilizador Não Registado

---


**Lugares**

*  Endereço Postal
*  Plataforma

---

**Eventos**

* Processo de Seriação
* Candidatura

---


**Objectos físicos**

*

---


**Especificações e descrições**

*  Área de Atividade
*  Competência Técnica
*  Categoria (de Tarefa)
*  Tarefa
*  Tipo Regimento
*  Grau Proficiência
*  Habilitação Académica
*  Reconhecimento de Competência Técnica
---


**Catálogos**

*

---


**Conjuntos**

* Lista de Competência Técnica Requeridas

---


**Elementos de Conjuntos**

*  Caráter de Competência Técnica Requerida

---


**Organizações**

*  T4J (Plataforma)
*  Organização

---

**Outros sistemas (externos)**

*  (Componente Gestão Utilizadores)
*  Algoritmo (Externo) Gerador de Passwords

---


**Registos (financeiros), de trabalho, contractos, documentos legais**

*  Tipo Regimento

---


**Instrumentos financeiros**

*

---


**Documentos referidos/para executar as tarefas/**

*

---



## **Racional sobre identificação de associações entre classes** ##

| Conceito (A) 		|  Associação   		|  Conceito (B) |
|----------	   		|:-------------:		|------:       |
| Administrativo  	| define    		 	| Área de Atividade  |
|   					| define            | Competência Técnica  |
|   					| trabalha para     | Plataforma  |
|						| atua como			| Utilizador |
| Plataforma			| tem registadas    | Organização  |
|						| tem/usa    			| Freelancer  |
|						| tem     			| Administrativo  |
| 						| possui     			| Competência Técnica  |
| 						| possui     			| Área de Atividade  |
| 						| possui     			| Categoria (de Tarefa)  |
|                       | recorre a         | Algoritmo (Externo) Gerador Passwords |
|                       | publicita         | Anuncio   |
|                       | suporta           | Tipo de Regimento|
|                       | **recorre**         | **Agendador** |    
| Competência Técnica| referente a       | Área de Atividade  |
|                    | aplica            | Grau Proficiência|
| Categoria (de Tarefa)| enquadra-se em | Área de Atividade  |
|						| tem 					| Lista de Competência Técnica Requeridas
| Lista de Competência Técnica Requeridas | tem | Caráter de Competência Técnica Requerida
| Caráter de Competência Técnica Requerida |  é referente a | Competência Técnica
|                                          | exige (como minimo)| Grau de Proficiência|
| Organização			| localizada em 	   | Endereço Postal  |
|						| tem gestor     	| Colaborador |
|						| tem		     		| Colaborador |
|						| possui		     	| Tarefa |
|                       | **realiza** | **Processo de Atribuição** |
|                       |  **atribui**  |**Adjudicação**|
| Tarefa		    	| enquadra-se	em 		| Categoria |
|       		    	| especificada por 	| Colaborador |
|                       | dá origem         | Anuncio  |
|                       | **executada após**    | **Adjudicação**   |    
| Anuncio               | publicita         | Tarefa |
|                       | rege-se           | Tipo de Regimento|
|                       | espoleta          | Processo de Seriação|
|                       | possui           | Candidatura|
|                       | publicado           | Colaborador|
| Processo de Seriação  | segundo regras establecidas por | Tipo de Regimento|
|                       | resulta            | Classificacao|
|                       | realizado por      | Colaborador|
|                       | **suporta**       | **Processo de Atribuição** |
| Classificacao         | referente a        | Candidatura|
| Candidatura           | realizado por       | Freelancer|
|                       | tem/recebe          | Classificacao|
| Gestor (de Organização)| é um (papel de)| Colaborador |
| Freelancer			| atua como			| Utilizador |
|                       | tem               | Habilitação Académica|
|                       | recebe(u)         | Reconhecimento de Competência Técnica |
|                       | tem               | Endereço Postal| 
|                       | efetua            | Candidatura| 
|                       | **recebe**        | **Adjudicao**|
|Reconhecimento de Competência Técnica| relativo a| Competência Técnica|
|                                     | reconhece| Grau Proficiência|
| Colaborador			| atua como			| Utilizador |
|                       | participa em      | Processo de Seriacao|
| Algoritmo (Externo) Gerador Passwords | gera password de | Utilizador |
| **Agendador** | **espoleta** | **Processo de Atribuição** |


## Modelo de Domínio

![MD.svg](MD.svg)


