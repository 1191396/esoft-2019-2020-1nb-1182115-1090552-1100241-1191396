﻿# UC13 -  Seriar (autmaticamente) Candidaturas de Anúncio

## 1. Engenharia de Requisitos

### Formato Breve
O Timer dá início ao processo de seriação automática. O Sistema valida os anuncios a seriar (anuncios que ainda nao foram seriados, que tem regimento com critérios de seriação objetivos e que se encontrem dentro do seu intervalo de seriação). O Sistema obtem a lista de candidaturas por cada anuncio. O Sistema classifica as candidaturas, com base no segundo valor mais baixo, pretendido pelo candidato. O sistema verifica se o tipo de seriação é com atribuição obrigatória e prossegue com o processo de atribuição. O sistema informa o sucesso da operação.

### SSD
![UC13_SSD.svg](UC13_SSD.svg)


### Formato Completo

#### Ator principal
Timer/Agendador -Criado pela classe Plataforma (Alternativa 1)

#### Partes interessadas e seus interesses
* **Organização:** pretende contratar pessoas externas (outsourcing) para a realização de determinadas tarefas e com competências técnicas apropriadas.
* **Freelancer:** pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* **T4J:** pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa. 


#### Pré-condições
O anúncio tem de estar criado e ser elegível.
A lista de candidaturas elegíveis para esse anúncio tem de estar cridada.

#### Pós-condições
A informação do processo de seriação é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O Timer dá início ao processo de seriação automática. 
2. O Sistema valida os anuncios a seriar (anuncios que ainda nao foram seriados, que tem regimento com critérios de seriação objetivos e que se encontrem dentro do seu intervalo de seriação).
3. O Sistema obtem a lista de candidaturas por cada anuncio.
4. O Sistema classifica candidatura, com base no valor, pretendido pelo candidato.
5. O passo 4 repete-se enquanto não forem classificadas todas as candidaturas do anúncio.
6. O sistema verifica se o tipo de seriação é com atribuição obrigatória e prossegue com o processo de atribuição.
7. Os passos 3 a 6 repetem-se enquanto houverem anúncios a serem seriados.
8. O sistema informa o sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. No arranque do processo de seriação não existem anúncios elegíveis para serem seriados.

> O caso de uso termina.

2a. O sistema deteta que a lista de candidaturas está vazia.
>
	> O caso de uso termina.

6a. Tipo de seriação é com atribuição opcional. 
> 	1. O sistema pausa o processo e este fica á espera para ser continuado pelo Gestor de Organização (UC14) e inicia nova seriação de um anúncio caso haja outros a serem seriados. 
> 
	> O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
\-
* Qual a frequência de ocorrência deste caso de uso?
* O mesmo lugar/classificação pode ser atribuído a mais do que uma candidatura (e.g. em caso de empate)?
* O processo de seriação pode ser concluído havendo candidaturas por classificar?
* Há algum motivo que possa levar à desclassificação de uma candidatura?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC13_MD.svg](UC13_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  				  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Timer dá início ao processo de seriação automática.   	|	... interage com o Timer? | Plataforma | IE: Alternativa 1  |
| 															| ...seria Anuncios Automaticamente | RegistoAnuncio | IE: no MD Plataforma conhece Anuncios e delega o RegistoAnuncio, por aplicação HC+LC |
||...possui o RegistoAnuncio|Plataforma|IE: Plataforma tem anuncios |
| 																	|   ...conhece todos os dados do Anuncio?	| Anuncio	| IE: Anuncio conhece os seus proprios dados||
| 2. O Sistema valida os anuncios a seriar. |	...conhece os anúncios existentes a listar?	| RegistoAnuncio     |	IE: RegistoAnuncio tem/agrega os anuncios daquela Plataforma	|
| 3. O Sistema obtem a lista de candidaturas por cada anuncio.  |...conhece as candidaturas de cada Anuncio	|ListaCandidaturas	 |no MD Anuncio conhece Candidaturas e delega a ListaCandidatura, por aplicação HC+LC |
||...possui ListaCandidaturas|Anuncio|IE: Anuncio tem candidaturas|||
| 4. O Sistema classifica candidatura, com base no valor, pretendido pelo candidato.  |...cria a instância Classificacao?| ProcessoSeriacao| Creator: Processo Seriacao agrega instâncias de Classificação |
||...possui ProcessoSeriacao?|Anuncio|IE: Anuncio tem ProcessoSericacao|
||...guarda os dados intrudozidos?| Classificacao| IE: Classificacao guarda os seus próprios dados|
||...valida os dados introduzidos?| ProcessoSeriacao|IE: ProcessoSeriacao agrega instâncias de Classificação|
| 5. O sistema verifica se o tipo de seriação é com atribuição obrigatória e prossegue com o processo de atribuição. |...conhece os requisitos da atribuição?|TipoRegimento|IE: TipoRegimento tem as regras de ordenação de candidatura | 
||...possui TipoRegimento|ProcessoSeriacao|IE: ProcessoSeriacao tem tipoRegimento|  
||...responsável pela adjudicacao da tarefa?| Plataforma| IE: Plataforma responsável pela atribuição automática| 
| 6. O sistema informa o sucesso da operação. |	||

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Classificacao
 * Anuncio
 * TipoRegimento
 * ProcessoSeriacao
 * Candidatura
 * Agendador/Timer
 * Freelancer
 * Tarefa

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RegistoAnuncio
 * ListaCandidatura
 * ListaTarefas

###	Diagrama de Sequência

![UC13_SD_Timer.svg](UC13_SD_Timer.svg)

![UC13_SD.svg](UC13_SD.svg)

 * Diagrama de Sequência Secundário - IUC_adjudicacaoDeTarefa<br><br>
![IUC_adjudicacaoDeTarefa](IUC_adjudicacaoDeTarefa.svg)

###	Diagrama de Classes

![UC13_CD.svg](UC13_CD.svg)
