# UC9 - Efetuar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer inicia a candidatura a um anúncio. O Sistema retorna uma lista de anúncios ao qual o freelancer é elegível. Este seleciona um único anuncio. De seguida, a plataforma pede-lhe dados de preenchimento obrigatório tais como o valor pretendido pela realização da tarefa e o número de dias necessários à sua realização. O freelancer introduz a informação pretendida. De seguida, a plataforma solicita dados opcionais (texto de apresentação e/ou motivação) ao qual o freelancer poderá ou nao introduzir. à posterior, o sistema apresenta os dados e solicita a confirmação do freelancer. O freelancer confirma e a plataforma retorna a informação da operação bem sucedida

### SSD
![UC9_SSD.svg](UC9_SSD.svg)

### Formato Completo

#### Ator principal
Freelancer

#### Partes interessadas e seus interesses
* **Colaborador de Organização:** Pretende receber Candidaturas aos Anúncios publicados.
* **Organização:** Pretende receber Candidaturas para as Tarefas publicadas para execução por Freelancers.
* **Freelancer:** Pretende candidatar-se aos anuncios disponíveis na plataforma que melhor se adaptem às suas capacidades.
* **T4J:** Pretende que os anuncios sejam entregues aos profissionais que melhor se qualifiquem para a realização da tarefa pretendida.

#### Pré-condições
\-

#### Pós-condições
* É registada uma nova Candidatura a um Anúncio.

### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer inicia a candidatura a um anúncio. 
2. O sistema solicita a escolha do anúncio duma lista de anúncios em período de candidatura e para os quais o Freelancer está elegível.
3. O Freelancer identifica o anúncio para o qual pretende apresentar uma candidatura.
4. O sistema solicita os dados necessários para a candidatura ao anúncio (i.e. valor pretendido, numero dias, texto apresentacão(opcional), texto motivação(opcional)).
5. O freelancer introduz os dados obrigatórios e **(opcional)** dados opcionais. 
6. A plataforma apresenta os dados e solicita a confirmação. 
7. O freelancer confirma.
8. O sistema regista uma nova candidatura ao anúncio e informa o Freelancer do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O Freelancer solicita o cancelamento da candidatura.

> O caso de uso termina.

2a. O sistema deteta que a lista de anúncios está vazia.
>	1. O sistema informa o freelancer de tal facto.
>
	> O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
> 1. O sistema informa quais os dados em falta. 
> 2. O sistema permite a introdução dos dados em falta (passo 4).
> 
	> 2a. O freelancer não altera os dados. O caso de uso termina. 

6b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o freelancer para o facto. 
> 2. O sistema permite a sua alteração (passo 4 e passo 6).
> 
	> 2a. O freelancer não altera os dados. O caso de uso termina. 
 
#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* O Freelancer pode apresentar mais do que uma candidatura ao mesmo anúncio?
* Qual é a informação do anúncio que o Freelancer pode visualizar? Toda?
* O Freelancer pode consultar as candidaturas efetuadas por outros Freelancers?
* Depois de submeter uma candidatura, o Freelancer pode atualizar e/ou retirar a mesma?
* A candidatura pode ser realizada fora do prazo (i.e. período de candidatura)?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC
![UC9_MD.svg](UC9_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Freelancer inicia a candidatura a AnÃºncio |... interage com o utilizador?|EfectuarCandidaturaUI	|Pure Fabrication|
| |...coordena o UC?	 				| EfectuarCandidaturaController	| Controller    |Pure Fabrication|
| |...cria instância de Candidatura? 			| Anúncio | O Anúncio recebe Candidaturas|
| |							| ListaCandidaturas | O Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas|
| |...conhece o utilizador/Freelancer a usar o sistema?	|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
| |...conhece o Freelancer ?				|Plataforma|conhece todos os Freelancers|
| |				|RegistoFreelancer|Por aplicação de HC+LC delega a RegistoFreelancer|
| 2. O sistema apresenta os Anúncio elegí­veis para o Freelancer e pede que seja selecionado um.|... conhece as anuncios? |Plataforma|IE: no MD a Plataforma possui Anúncios. |
| | 						     | RegistoAnuncios	|IE: no MD a Plataforma possui Anuncios. Por aplicação de HC+LC delega a RegistoAnuncios|
| |...conhece os Anúncio elegiveis para o Freelancer| Freelancer 	| O Freelancer possui Reconhecimentos de Competências Técnicas | IE |  
| | 						     | Reconhecimento	| O Reconhecimento confirma o GrauProficiencia do freelancer numa Competencia Tecnica | IE |  
| | 						     |Anúncio	| O Anúncio é relativo a uma Tarefa | IE: no MD o Anúncio relativo a uma Tarefa |
| | 						     | Tarefa		| A Tarefa tem uma Categoria de Tarefa associada | IE: no MD a Tarefa é relativa a uma Categoria de Tarefa |
| | 						     | Categoria | A Categoria de Tarefa possui Carácter das Competencias Tecnicas | IE: no MD a Categoria de Tarefa possui CarácterCT |
| | 						     | CarácterCT | O Carácter de Competencia Tecnica possui GrauProficiencia minino para cada CT e obrigatoriedade dessa CT | IE: no MD o CaracterCT possui GrauProficiencia mi­nimo e obrigatoriedade|
| | 						     | GrauProficiencia | A Competencia Tecnica possui os Graus de Proficiencia para cada Competencia Tecnica | IE |
| 3. O Freelancer seleciona um Anuncio elegi­vel. | | | |
| 4. O sistema solicita os dados necessarios para a candidatura (i.e. valor pretendido, numero dias, texto apresentacao(opcional), texto motivação(opcional)) |N/A|||
| 5. O Freelancer introduz os dados solicitados. | ... guarda os dados introduzidos?|Anuncio| No MD Anuncio recebe Candidaturas|
| |							| ListaCandidaturas | Por aplicaçao de HC+LC delega a ListaCandidaturas|
| |							| Candidatura | IE: Candidatura conhece os seus dados|
| 6. O sistema valida e apresenta os dados ao Freelancer e pede a sua confirmaçao.|	... valida os dados da Candidatura (validaçao local)?|Candidatura| IE: possui os seus proprios dados.|
| |	... valida os dados da Candidatura (validaçao global)?| ListaCandidaturas| IE: no MD o Anuncio recebe Candidaturas. Por aplicaçao de HC+LC delega a ListaCandidaturas|
| 7. O Freelancer confirma. | N/A|||
| 8. O sistema regista os dados e cria a Candidatura e informa o Freelancer do sucesso da operaçao. |...guarda a Candidatura?| Anuncio|IE: no MD o Anuncio recebe Candidaturas.|
| |							| ListaCandidaturas | IE: no MD o Anuncio recebe Candidaturas. Por aplicaçao de HC+LC delega a ListaCandidaturas|
| |...informa o colaborador?|EfectuarCandidaturaUI|
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:
* Plataforma
* Candidatura
* Freelancer
* Reconhecimento
* Grauproficiência
* Anúncio
* Tarefa
* Categoria
* CáracterCT
* CompetênciaTécnica

Outras classes de software (i.e. Pure Fabrication) identificadas:
* EfectuarCandidaturaUI
* EfectuarCandidaturaController
* RegistoFreelancer
* RegistoAnuncios
* ListaCandidaturas

###	Diagrama de Sequência

![UC9_SD.svg](UC9_SD.svg)

###	Diagrama de Classes
![UC9_CD.svg](UC9_CD.svg)




