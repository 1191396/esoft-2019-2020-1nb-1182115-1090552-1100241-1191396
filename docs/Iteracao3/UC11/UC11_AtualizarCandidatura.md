# UC11 - Atualizar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve

O Freelancer inicia a atualização de uma candidatura a um anúncio. O sistema mostra a lista de anúncios ao qual o freelancer se candidatou e que ainda esteja dentro do prazo para o fazer. O Freelancer identifica o anúncio para o qual pretende atualizar a candidatura. O sistema apresenta os dados da candidatura. O Freelancer introduz a atualização dos dados facultativamente. O sistema valida, apresenta os dados ao Freelancer e pede a sua confirmação. O Freelancer confirma os dados. O sistema regista as alterações feitas à candidatura escolhida pelo Freelancer e o informa do sucesso da operação.

### SSD
![UC11_SSD.svg](UC11_SSD.svg)

### Formato Completo

#### Ator principal
Freelancer

#### Partes interessadas e seus interesses
* **Freelancer:** Pretende atualizar a candidatura previamente submetida.
* **Organização:** Pretende que as informações das candidaturas estejam atualizadas.  
* **T4J:** Pretende que as informações das candidaturas estejam atualizadas.  

#### Pré-condições
* O Freelancer terá de ter, previamente, uma candidatura efetuada e que a mesma esteja num período válido.

#### Pós-condições
\-

### Cenário de sucesso principal (ou fluxo básico)

1. O Freelancer inicia a atualização de uma candidatura a um anúncio.
2. O Sistema mostra a lista de candidaturas que o freelancer efetuou **e que se encontra em período válido**.
3. O Freelancer identifica a candidatura para o qual pretende atualizar os dados.
4. O sistema apresenta os dados da candidatura escolhida.
5. O Freelancer atualiza os dados facultativamente.
6. O sistema valida, apresenta os dados ao Freelancer e pede a sua confirmação.
7. O Freelancer confirma os dados.
8. O sistema regista as alterações feitas à candidatura escolhida pelo Freelancer e o informa do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O Freelancer solicita o cancelamento da atualização da candidatura a um anúncio.
> O caso de uso termina.

2a. Não existem candidaturas disponíveis para atualizações em período válido.
> O caso de uso termina.

2b. Não existem candidaturas registadas.
> O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 5)
>
	> 2a. O freelancer não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
>   1. O sistema alerta o Freelancer para o facto.
>   2. O sistema permite a sua alteração (passo 5).
>
	> 2a. O Freelancer não altera os dados. O caso de uso termina.
 
#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

\-

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC
![UC11_MD.svg](UC11_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O freelancer inicia a atualizacao de uma candidatura a um anunncio. |... interage com o utilizador?	|AtualizarCandidaturaUI			|Pure Fabrication|
|  																	|	... coordena o UC?			| AtualizarCandidaturaController 	| Controller    |
| 																	|... conhece o utilizador a usar o sistema?|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|	
| 																	|... conhece todos freelancers?|RegistoFreelancers|IE: no MD a Plataforma possui Freelancer. Por aplicação de HC+LC delega a RegistaFreelancer|	
|																	|... conhece todos os anúncios no sistema? | RegistoAnuncios | IE: no MD a Plataforma possui Anuncio. Por aplicação de HC+LC delega a RegistaAnuncio |
|																	|... conhece as candidaturas? | Anuncio | IE: Cada Anuncio tem uma ListaCandidaturas |
| 2. O sistema apresenta **as candidaturas realizadas por ele aos anúncios que se encontram em perí­odo válido de apresentação de candidaturas**. |... conhece o perí­odo válido de apresentação? | Anuncio | IE: Anuncio possui todos os seus perí­odos |
| 3. O Freelancer seleciona um Anúncio elegível. | (N/A) | | |
| 4. O sistema solicita dados obrigatórios (valor pretendido e nº dias necessários) e opcional (texto de apresentação e/ou motivação). | ... quem conhece as candidaturas? | Anúncio | O Anúncio tem uma lista de candidaturas.
| | ... quem executa a cópia da candidatura? | ListaCandidaturas | A ListaCandidaturas agrega todas as candidaturas por aplicação de HC+LC.
| 5. O Freelancer introduz os dados solicitados. | ... guarda os dados introduzidos?|Anúncio| No MD Anúncio recebe Candidaturas|
| |							| ListaCandidaturas | Por aplicação de HC+LC delega a ListaCandidaturas|
| |							| Candidatura | IE: Candidatura conhece os seus dados|
| 6. O sistema valida e apresenta os dados ao Freelancer e pede a sua confirmação.|	... valida os dados da  Candidatura cópia (validação local)?|Candidatura| IE: possui os seus próprios dados.|
| |	... valida os dados da Candidatura cópia (validação global)?| ListaCandidaturas| IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas|
| 7. O Freelancer confirma. | (N/A) |||
| 8. O sistema informa da operação executada com sucesso. |...atualiza a Candidatura?| Anúncio |IE: no MD o Anúncio recebe Candidaturas.|
| |							| ListaCandidaturas | IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas|
| |...informa o colaborador? | CandidaturaAnuncioUI |
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:
 * Plataforma
 * Candidatura
 * Freelancer
 * Reconhecimento
 * Grauproficiência
 * Anúncio
 * Tarefa
 * Categoria
 * CáracterCT
 * CompetênciaTécnica

Outras classes de software (i.e. Pure Fabrication) identificadas:
* AtualizarCandidaturaUI
* AtualizarCandidaturaController
* RegistoFreelancers
* RegistoAnuncios
* ListaCandidaturas
###	Diagrama de Sequência

![UC11_SD.svg](UC11_SD.svg)
* Diagrama de Sequência Secundário - IUC_getEmailByLogin
![IUC_getEmailByLogin.svg](IUC_getEmailByLogin.svg)
* Diagrama de Sequência Secundário - IUC_getListCandidaturasByFreelancer
![IUC_getListCandidaturasByFreelancer.svg](IUC_getListCandidaturasByFreelancer.svg)

###	Diagrama de Classes
![UC11_CD.svg](UC11_CD.svg)




