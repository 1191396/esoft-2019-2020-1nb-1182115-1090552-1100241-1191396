# Diagrama de Casos de Uso

![Diagrama de Casos de Uso](DUC.svg)

# Casos de Uso
| UC  | Descrição                                                               | It1   | It2   | It3   |                  
|:----|:------------------------------------------------------------------------|-------|-------|-------|
| UC1 | [Registar Organização](UC1/UC1_RegistarOrganizacao.md)                          |   C   |   R   |   -   |
| UC2 | [Definir Área de Atividade](UC2/UC2_DefinirArea.md)                             |   C   |   I   |   -   |
| UC3 | [Definir Categoria (de Tarefa)](UC3/UC3_DefinirCategoria.md)                    |   C   |   R   |   -   |
| UC4 | [Especificar Competência Técnica](UC4/UC4_EspecificarCT.md)                     |   C   |   R   |   -   |
| UC5 | [Especificar Colaborador de Organização](UC5/UC5_EspecificarColaborador.md)     |   C   |   R   |   -   |
| UC6 | [Especificar Tarefa](UC6/UC6_EspecificarTarefa.md)                              |   C   |   R   |   -   |
| UC7 | [Registar Freelancer](UC7/UC7_RegistarFreelancer.md)                            |   -   |   C   |   -   |
| UC8 | [Publicar Tarefa](UC8/UC8_PublicarTarefa.md)                                    |   -   |   C   |   R   |
| UC9 | [Efetuar Candidatura a Anúncio](UC9/UC9_EfetuarCandidatura.md)                            |   -   |   C   |   R   |
| UC10 | [Seriar (manualmente) Candidaturas de Anúncio](UC10/UC10_SeriarAnuncio.md)                                   |   -   |   C   |   R   |
| UC11 | [Atualizar Candidatura Submetida](UC11/UC11_AtualizarCandidatura.md)                         |   -   |   -   |   C   |
| UC12 | [Retirar Candidatura Submetida](UC12/UC12_RetirarCandidatura.md)                           |   -   |   -   |   C   |
| UC13 | [Seriar (automaticamente) Candidaturas de Anúncios](UC13/UC13_SeriarAutomaticamenteCandidaturasAnuncios.md)    |   -   |   -   |   C   |
| UC14 | [Adjudicar/Atribuir (manualmente) Anúncio](UC14/UC14_AdjudicarAnuncio.md)                             |   -   |   -   |   C   |

C - Criado |
R - Refinado |
I - Inalterado | 