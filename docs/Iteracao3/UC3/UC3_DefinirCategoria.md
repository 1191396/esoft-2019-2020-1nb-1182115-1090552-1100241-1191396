# UC3 - Definir Categoria (de Tarefa)

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia definição de uma nova categoria de tarefas. O sistema solicita os dados necessários (i.e. código e nome). O administrativo introduz os dados solicitados. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

### SSD
![UC3_SSD.svg](UC3_SSD.svg)

### Formato Completo

#### Ator principal
Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** Pretende definir novas categorias tarefas para que as organizações e freelancers sejam encontrados mais facilmente através dessas tarefas que definiram como funções a desempenhar.
* **T4J:** Pretende que a plataforma permita associar as categorias de tarefas às áreas de atividades e às tarefas.

#### Pré-condições
-n/a

#### Pós-condições
A informação da categoria de tarefas é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a definição de uma nova categoria de tarefa. 
2. O sistema solicita os dados necessários (descrição). 
3. O administrativo introduz os dados solicitados. 
4. O sistema a lista de áreas de atividades.
5. O administrativo seleciona a área de atividades
6. O sistema mostra a **lista de competências técnicas referentes à área de atividade previamente selecionada** para que seja selecionada uma.
7. O administratativo seleciona as competencias técnicas
8. O sistema mostra os graus de proficiência aplicáveis a essa competência técnica e pede para selecionar o grau mínimo exigível bem como a sua obrigatoriedade.
9. O administrador seleciona o grau pretendido e define a obrigatoriedade da competência técnica selecionada. 
10. ** Os passos 7 a 9 repetem-se enquanto não forem introduzidas todas as competências técnicas pretendidas.**
11. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
12. O administrativo confirma. 
13. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da definição de uma categoria de tarefas.

> O caso de uso termina.

4a. O sistema deteta que a lista de áreas de atividades está vazia.
>	1. O sistema informa o administrativo de tal facto.
>	2. O sistema permite a definição de uma nova área de atividade (UC2). 
>
	>	2a. O administrativo não define uma área de atividade. O caso de uso termina.

5a. OO administrativo não encontra a área de atividade pretendida.
>	1. O administrativo informa o sistema de tal facto.
>	2. O sistema permite a definição de uma nova área de atividade (UC2).
>
	>	2a. O administrativo não define uma área de atividade. O caso de uso termina.

6a. O sistema deteta que a lista de competências técnicas está vazia.
> 1. O sistema informa o administrativo de tal facto. 
> 2. O sistema permite a especificação de uma nova competência técnica (UC4).
> 
	> 2a. O administrativo não especifica uma competência técnica. O caso de uso termina.

7a. O administrativo não encontra a competência técnica pretendida.
> 1. O sistema informa o administrativo de tal facto. 
> 2. O sistema permite a especificação de uma nova competência técnica (UC4).
> 
	> 2a. O administrativo não especifica uma competência técnica. O caso de uso termina.


11a. Dados mínimos obrigatórios em falta.
> 1. O sistema informa quais os dados em falta. 
> 2. O sistema permite a introdução dos dados em falta (passo 3).
> 
	> 2a. O administrativo não altera os dados. O caso de uso termina.

11b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
> 1. O sistema informa o administrativo de tal facto. 
> 2. O sistema permite a sua alteração (passo 3).
> 
	> 2a. O administrativo não altera os dados. O caso de uso termina. 

11c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o administrativo para o facto. 
> 2. O sistema permite a sua alteração (passo 3).
> 
	> 2a. O administrativo não altera os dados. O caso de uso termina. 
 
#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* O identificador da categoria de tarefa tem que obedecer a alguma regra (e.g. ser sequencial)?
* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* Qual o número de competências técnicas que no mínimo são obrigatórias para as categorias?
* Qual a frequência de ocorrência deste caso de uso?
* O grau de proficiência mínimo por prefefinição é obrigatório?  

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC
![UC3_MD.svg](UC3_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia a definição de uma nova Categoria de tarefa.   		 |	... interage com o utilizador? | DefinirCategoriaTrarefaUI    | PureFabrication |
|  		 |	... coordena o UC?	| DefinirCategoriaTrarefaController | Controller    |
|  		 |	... cria instância de CompetenciaTecnica| Plataforma   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários (Descrição).  		 |							 |             |                              |
| 3. O administrativo introduz os dados solicitados.  		 |	  |    |      |
| 4. O sistema apresenta a lista de Áreas de atividade | ...quem tem conhecimento das Áreas de atividade? | Plataforma | IE: A Plataforma tem todas as áreas de atividades instanciadas|
| 5. O administrativo seleciona uma área de atividade. | ...guarda o ID da área de atividade? | CategoriaTarefa | IE: A Lista de categorias é relacionada com o ID da Área de atividade |
| 6. O sistema valida e apresenta uma lista de Competencias Técnicas. | ...quem tem conhecimento das Competências Técnicas? | Plataforma |  IE:A Plataforma tem todas as Competências Técnicas.|
|		| ...valida os dados das Competências Tecnicas? (validação local)? | Plataforma | IE: A Plataforma tem todas as competências Técnicas e todas as áreas de Atividade. |
| 7. O admistrativo seleciona uma Competencia Técnica. | |||||
| 8. O sistema solicita obrigatoriedade da Competência Técnica. ||||||
| 9. O adminstrativo introduz a instrução. | ...guarda a competência Ténica, obrigatoriedade e os restantes dados introduzidos | CategoriaTarefa | IE: A CategoriaTarefa possui 0 ou mais Competências técnicas. |
| 10. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. |	...valida os dados da Categoria (validação local) | CategoriaTarefa |IE - Possui os seu próprios dados.|
|	 |	...valida os dados da Categoria (validação global) | Plataforma | IE - A Plataforma tem/agrega CategoriaTarefa |
| 11. O adminsitrativo confirma. ||||||
| 12. O sistema regista os dados e informa o administrativo do sucesso da operação.  		 |	... guarda a CategoriaTarefa criada? | Plataforma  | IE: No MD a Plataforma possui CompetenciaTecnica|  
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * AreaAtividade
 * CategoriaTarefa

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirCategoriaTarefaUI
 * DefinirCategoriaTarefaController


###	Diagrama de Sequência

![UC3_SD.svg](UC3_SD.svg)

###	Diagrama de Classes
![UC3_CD.svg](UC3_CD.svg)




