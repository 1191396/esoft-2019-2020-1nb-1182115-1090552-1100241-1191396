# UC 14 - Adjudicar/Atribuir (manualmente) Anúncio

## 1. Engenharia de Requisitos

### Formato Breve

O gestor da organização inicia a atribuição manual. O sistema apresenta a lista de anúncios seriados manualmente e com atribuição opcional. O gestor da organização seleciona um anúncio. O sistema apresenta a lista de classificação ao gestor da organização, e mostra-lhe qual candidatura é a mais indicada para a tarefa. O gestor da organização seleciona uma candidatura. O sistema cria uma adjudicação, apresenta os dados e solicita confirmação. O gestor da organização confirma. O sistema informa o sucesso da operação.


### SSD
![UC14-SSD](UC14_SSD.svg)


### Formato Completo

#### Ator principal

Gestor da Organização

#### Partes interessadas e seus interesses

* **Organização:** pretende que a tarefa seja executada.
* **Gestor da Organização:** pretende adjudicar/atribuir um anúnico a um Freelancer.
* **Freelancer:** pretende receber a atribuição/adjudicação de um anúncio.
* **TJ4:** pretende a adjudicação de tarefas a freelancers.

#### Pré-condições

* Devem existir anúncios seriados manualmente e com atribuição opcional.

#### Pós-condições

* Cria uma adjudicação de uma tarefa em que conste: organização que adjudica a tarefa, o freelancer aquem é adjudicada a realização da tarefa, a descrição da tarefa, o período afeto da realização da mesma o valor remuneratório aceite por ambas as partes e uma referência ao anúncio que lhe deu origem.


#### Cenário de sucesso principal (ou fluxo básico)


1. O gestor da organização inicia o processo de atribuição manual. 
2. O sistema apresenta a lista de anúncios seriados manualmente e com atribuição opcional. 
3. O gestor da organização seleciona um anúncio.
4. O sistema obtém a candidatura selecionada à realização da tarefa, valida, apresenta os dados e solicita ao gestor da organização que confirme. 
5. O gestor da organização confirma a adjudicação ao candidato.
6. O sistema informa o sucesso da operação. 


#### Extensões (ou fluxos alternativos)

1a. O sistema verifica que não há aúncios seriados com atribuição opcional a serem adjudicados.
>   1.O sistema informa ao gestor.
>   2.O caso de uso termina


4b. O gestor rejeita o candidato selecionado
>   O caso de uso termina.



#### Requisitos especiais
(enumerar requisitos especiais aplicaveis apenas a este UC)

\-

#### Lista de Variações de Tecnologias e Dados
(enumerar variações de tecnologias e dados aplicaveis apenas a este UC)
\-

#### Frequência de Ocorrência
(descrever com que frequência este UC é realizado)

\-

#### Questões em aberto

1. Deve ser enviado um alerta ao Freelancer informado-o da adjudicação da tarefa? 

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC14-MD](UC14_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O gestor da organização inicia o processo de atribuição manual de uma tarefa. 		 |	... interage com o gestor da organização? | AdjudicarAnuncioUI  | Pure Fabrication   |
| |... coordena a UC? | AdjudicarAnuncioController | Controller |
| |... conhece o gestor a usar o sistema? | SessaoUtilizador | IE: cf documentação do componente de gestão de utilizadores. |
| |... sabe a que organização o gestor pertence? | Plataforma | IE: conhece todas organizações. |
|||  RegistoOrganizacoes | IE: IE: no MD Plataforma conhece organizações e delega a RegistoOrganizacoes, por aplicação HC+LC. |
| | | Organizacoes | IE: conhece os seus colaboradores |
|2. O sistema apresenta a lista de anúncios seriados manualmente e com atribuição opcional.  | ...conhece anúncios do colaborador a serem atribuídos manualmente?	 | Plataforma | IE: no MD a Plataforma possui Anúncios. |
| | | RegistoAnuncio | IE: no MD a Plataforma possui Anúncios. Por aplicação LC+HC delega a RegistoAnuncios |
| | | ListaAnuncios | IE: conhece o tipo de regimento do anúncio e sabe quais foram seriados. |
| 3. O gestor da organização seleciona um anúncio.  		 ||||
| 4. O sistema obtém a candidatura selecionada à realização da tarefa, valida, apresenta os dados e solicita ao gestor da organização que confirme.	 |	...conhece o candidato selecionado?| Anuncio |IE: Tem lista de candidatos e, no MD, espoleta o processo de sriação.      |
|||ProcessoSeriacao | IE: Tem lista de classificação |
|||ListaClassificacao | IE: sabe qual foi o primeiro classificado da lista.|
|||Classificacao | IE: Tem candidato e ordem da classificação. |
||| Candidatura |IE: conhece Freelancer que candidatou-se ao anúncio.|
|||Freelancer | IE: tem os seus dados. |
||...conhce a descrição da tarefa adjudicada? | Tarefa | IE: tem dados de uma tarefa. |
||...conhece o período afeto à realização da tarefa? | Anuncio | IE: conhece o período de seriacao do anúncio. |
|||Candidatura | IE: sabe número de dias necessários para realização da tarefa. |
|||Adjudicacao | IE: sabe quando foi adjudicada a tarefa. |
||...conhece o valor remuneratório? |||
| |...cria instância de Adjudicacao? | Anuncio | Create(Regra3): Anúncio tem/conhece os dados usados para criar Adjudicacao|
||...valida os dados de adjudicação (validação local)?|Adjudicacao | IE: tem/conhece os próprios dados. |
||...valida os dados de Adjudicacao?(validação global)|Plataforma|IE: Tem agrega objetos do tipo Adjudicacao.|
| 5. O gestor da organização confirma a adjudicação ao candidato.  		 |  							 |             |                              |
| 6. O sistema informa o sucesso da operação.  		 |...guarda a adjudicação? | Organizacao |IE: no MD Organizacao atribui Adjudicacao.|
||...notifica o utilizador? |EspecificarCategoriaUI|            

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organização
 * Colaborador
 * Tarefa
 * Anuncio
 * Candidatura
 * Freelancer
 * Adjudicacao
 * ProcessoSeriacao
 * Classificacao 
 * SessaoUtilizador
 

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AdjudicarTarefaUI  
 * AdjudicarTarefaController
 * RegistoOrganizacoes
 * 


###	Diagrama de Sequência

![UC14-SD](UC14_SD.svg)


###	Diagrama de Classes

![UC14-CD](UC14_CD.svg)
