# UC5 - Especificar Colaborador de Organização

## 1. Engenharia de Requisitos

### Formato Breve

O Gestor da Organização solicita um registo para um novo colaborador da organização. O sistema solicita os dados necessários sobre o colaborador (exemp. nome, função, telefone e e-mail). O Gestor introduz os dados solicitados. O sistema valida e apresentda os dados, pedindo a sua confirmação. O Gestor da organização confirma. O sistema regista os dados do novo colaborador da organização, ficando este último registado no sistema e procede a informar o Gestor da Organização do sucesso da operação. 


### SSD
![UC5-SSD](UC5_SSD.svg)

### Formato Completo

#### Ator principal

Gestor da Organização

#### Partes interessadas e seus interesses

* **Gestor da Organização:** pretende que os colaboradores por ele registados especifiquem tarefas para a organização.
* **Colaborador de Organização:** pretende ser registado no sistema para poder especificar tarefas.
* **Organização:** pretende receber as tarefas especificadas pelo colaborador para, posteriormente, publicá-las e gerir o seu processo de adjudicação.
* **Freelancers:** pretende aceder ás tarefas publicadas de maneira a se poderem propôr a realizar-las. 

#### Pré-condições

-Organização estar registada no sistema.
-Gestor da Organização já se encontrar registado no sistema.


#### Pós-condições

-A informação sobre o novo colaborador da organização é registada no sistema.


#### Cenário de sucesso principal (ou fluxo básico)

1. O Gestor da Organização solicita um novo registo para um colaborador da organização.
2. O sistema solicita os dados necessários sobre o colaborador (exemp. nome, função, telefone, e-mail e password).
3. O Gestor da Organização introduz os dados solicitados.
4. O sistema valida e apresentda os dados, pedindo a sua confirmação.
5. O Gestor da organização confirma.
6. O sistema regista os dados do novo colaborador da organização, ficando este último registado no sistema e procede a informar o Gestor da Organização do sucesso da operação.


#### Extensões (ou fluxos alternativos)

(alternativas globais ao fluxo principal ou especifica de um determiando passo)

*a. O Gestor da Organização solicita o cancelamento do registo do novo colaborador da organização.

> O caso de uso termina.

4a. E-mail já registado no sistema.
>	1. O sistema informa que o e-mail já se encontra registado no sistema.
>	2. O sistema permite a introdução do novo e-mail (passo 3)
>
	>	2a. O Gestor da Organização não altera os dados. O caso de uso termina.

4b. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O Gestor da Organização não altera os dados. O caso de uso termina.

4c. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o Gestor da Organização para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O Gestor da Organização não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Outros dados obrigatórios no registo?
* O Gestor cria uma password para cada utilizador ou o sistema automaticamente gera uma e envia para o e-mail do novo colaborador?
* Caso ponto acima seja afirmativo, a password tem requisitos especiais?
* Sistema de prevenção para evitar contas com dados duplicados?
* Necessidade do sistema questionar qual a organização para registar o utilizador? Ou este verifica a organização através dos dados do Gestor de Organização?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC5_MD.svg](UC5_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Gestor de Organização inicia o processo de registo de um novo colaborador.|... interage com o gestor?|RegistarColaboradorUI|Pure Fabrication|
| |... coordena o UC?| RegistarColaboradorController |Controller|
| |... cria instâncias de Colaborador?|Organização|Creator(regra1)|
| 2. O sistema solicita os dados necessários sobre o colaborador (exemp. nome, função, telefone e e-mail).||||
| 3. O Gestor da Organização introduz os dados solicitados.|... guarda os dados introduzidos?|Colaborador|IE: instância criada no passo 1|
| |... cria instâncias de Colaborador?| Organização| creator(regra1)|
| 4. O sistema valida e apresentda os dados, pedindo a sua confirmação.|... valida os dados do Colaborador (validação local)|Colaborador|IE: possui os seus próprios dados|
||... valida os dados da Colaborador (validação global)|Organização|IE: A Organização tem registados Colaboradores|
| 5. O Gestor da organização confirma.||||
| 6. O sistema regista os dados do novo colaborador da organização, ficando este último registado no sistema e procede a informar o Gestor da Organização do sucesso da operação.|... guarda a Organizacao criada?|Plataforma|IE: No MD a Plataforma tem Organizacao|             
| |... regista/guarda os dados do novo Colaborador da Organizacao?|AutorizacaoFacade|IE. A gestão de utilizadores é responsabilidade do componente externo respetivo cujo ponto de interação é através da classe "AutorizacaoFacade"|

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organizacao
 * Colaborador
 * Plataforma

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * Registar ColaboradorUI  
 * Registar ColaboradorController


###	Diagrama de Sequência

![UC5_SD.svg](UC5_SD.svg)	


###	Diagrama de Classes

![UC5_CD.svg](UC5_CD.svg)
