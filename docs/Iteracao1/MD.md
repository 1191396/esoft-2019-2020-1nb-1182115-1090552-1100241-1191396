# Análise OO #

## Racional para identificação de classes de domínio ##

### _Lista de Categorias_ ###

**Transações (do negócio)**

* Pedido de registo de organização
* Pedido de definição de área de atividade
* Pedido de definição de categoria
* Pedido de especificação de competência técnica
* Pedido de registo de novo colaborador da organização
* Pedido de especificação de tarefa

---

**Linhas de transações**

* Desginação da organização
* Descrição da área de atividade
* Descrição da categoria
* Designação da competência técnica
* Designação do novo colaborador
* Descrição da tarefa

---

**Produtos ou serviços relacionados com transações**

* Organização registada
* Área de atividade criada
* Categoria de tarefa criada
* Competência técnica criada
* Novo colaborador registado
* Tarefa criada

---


**Registos (de transações)**

*  

---  


**Papéis das pessoas**

* Administrativo
* Freelancer
* Colaborador (de Organização)
* Gestor (de Organizacao)
* Utilizador
* Utilizador Não Registado

---


**Lugares**

*  Endereço Postal

---

**Eventos**

* 

---


**Objectos físicos**

*

---


**Especificações e descrições**

* Área de Atividade
* Competência Técnica
* Categoria de tarefa
* Tarefa

---


**Catálogos**

*  

---


**Conjuntos**

*  

---


**Elementos de Conjuntos**

*  

---


**Organizações**

*  T4J (Plataforma)
*  Organização

---

**Outros sistemas (externos)**

*  (Componente Gestão Utilizadores)


---


**Registos (financeiros), de trabalho, contractos, documentos legais**

* 

---


**Instrumentos financeiros**

*  

---


**Documentos referidos/para executar as tarefas/**

* 
---



###**Racional sobre identificação de associações entre classes**###

| Conceito (A) 		        |  Associação   		|  Conceito (B) |
|----------	   		        |:-------------:		|------:       |
| Plataforma			    | tem registadas        | Organização  |
|						    | tem/usa     			| Freelancer  |
|						    | tem     			    | Administrativo  |
| 						    | possui     			| Competência Técnica  |
| 						    | possui     			| Área de Atividade  |
| 						    | possui     			| Categoria de Tarefa  |
| 						    | possui     			| Tarefa  |
| Organizacao			    | localizada em         | Endereço Postal  |
|						    | tem     	            | Gestor de Organização |
|						    | possui		     	| Colaborador |    
| Administrativo  	        | define    		 	| Área de Atividade  |
|   					    | especifica            | Competência Técnica  |
|   					    | define                | Categoria de Tarefa  |
|						    | atua como			    | Utilizador |
| Gestor (de Organização)   | atua como             | Colaborador |
|                           | especifica            | Colaborador |
| Competência Técnica        | referente a           | Área de Atividade  |
| Freelancer			    | atua como			    | Utilizador |
| 			    | realiza		    | Tarefa |
| Colaborador			    | atua como			    | Utilizador |
|               		    | especifica    		| Tarefa     |
| Tarefa                  		    | possui    		    | Categoria de Tarefa      |
| Categoria de Tarefa	    | possui    		    | Competência Técnica     |
|                   	    | possui    		    | Área Atividade    |

## Modelo de Domínio

![MD.svg](MD_Global.svg)



