# UC6 - Especificar tarefa

## 1. Engenharia de Requisitos

### Formato Breve
O colaborador da organização dá início à especificação de uma nova tarefa. O sistema solicita os dados necessários ao colaborador (uma designação, uma descrição informal e outra de carácter técnico, uma estimativa de duração e custo. O sistema apresenta uma lista de categorias de tarefa. O colaborador seleciona a categoria de tarefa em que se enquadra a tarefa. O sistema valida e apresenta os dados ao colaborador, pedindo que os confirme. O colaborador confirma. O sitema regista os dados e informa o colaborador do sucesso da operação.


### SSD
![UC6_SSD.svg](UC6_SSD.svg)


### Formato Completo

#### Ator principal
Colaborador da Organização

#### Partes interessadas e seus interesses
* **Colaborador da Organização:** pretende especificar as tarefas para as quais, posteriormente, estas sejam publicadas pelas organizações respetivas.
* **T4J:** pretende que a tarefa seja especificada para posterior publicação pela organização.


#### Pré-condições
A categoria de tarefa tem que estar criada


#### Pós-condições
A especificação da tarefa é registada no sistema. 


### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador dá início à especificação de uma nova tarefa. 
2. O sistema solicita os dados necessários ao colaborador.
3. O colaborador introduz os dados solicitados. 
4. O sistema apresenta uma lista de categorias de tarefa.
5. O colaborador seleciona a categoria de tarefa em que a tarefa se enquadra.
6. O sistema valida e apresenta os dados ao colaborador, pedindo que os confirme. 
7. O colaborador confirma. 
8. O sistema regista os dados e informa o colaborador do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O colaborador solicita o cancelamento da especificação da tarefa.

> O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O colaborador não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o colaborador para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O colaborador não altera os dados. O caso de uso termina.

6c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 	1. O sistema alerta o colaborador para o facto. 
> 	2. O sistema permite a sua alteração (passo 3).
> 
	> 2a. O colaborador não altera os dados. O caso de uso termina. 

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
\-


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC6_MD.svg](UC6_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  				  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador dá início à especificação de uma nova tarefa. |	... interage com o colaborador? | DefinirTarefaUI |  PureFabrication |
|  |	... coordena o UC? | DefinirTarefaController |  Controller |
|  |	... fica responsável por criar instâncias de Tarefa? | Organizacao | Creator |
| 2. O sistema solicita os dados necessários ao colaborador. |		|     |		|
| 3. O colaborador introduz os dados solicitados. |	... guarda os dados introduzidos? | Tarefa | Information Expert (IE) - instância criada no passo 1 |
| 4. O sistema apresenta uma lista de categorias de tarefa. | ...conhece as categorias de tarefa existentes a listar? | Plataforma | IE - Plataforma tem/agrega todas as categorias |
| 5. O  colaborador seleciona a categoria de tarefa em que a tarefa se enquadra. | ...guarda a categoria selecionada? | Tarefa | IE - Tarefa tem/agrega uma categoria de tarefa |
| 6. O sistema valida e apresenta os dados ao colaborador, pedindo que os confirme. |	...valida os dados da Tarefa (validação local) | Tarefa |IE - Possui os seu próprios dados.|
|	 |	...valida os dados da Tarefa (validação global) | Organizacao | IE - A Organização tem/agrega Tarefa |
| 7. O colaborador confirma. | 		|     |		|
| 8. O sistema regista os dados e informa o colaborador do sucesso da operação. | ... guarda a Tarefa criada? | Organizacao  | IE - A Organização possui Tarefa |  
| | ... notifica o colaborador? | DefinirTarefaUI  | |  
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organizacao
 * CategoriaTarefa
 * Tarefa


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirTarefaUI  
 * DefinirTarefaController


###	Diagrama de Sequência

![UC6_SD.svg](UC6_SD.svg)


###	Diagrama de Classes

![UC6_CD.svg](UC6_CD.svg)
