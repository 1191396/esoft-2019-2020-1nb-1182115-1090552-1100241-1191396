# UC4 - Especificar Competência Técnica

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia a definição de uma competência técnica. O sistema solicita os dados necessários (i.e. código único e descrição breve e detalhada, e a que área de atividade está relacionada). O administrativo introduz os dados solicitados. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

### SSD
![UC4_SSD.svg](UC4_SSD.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende definir as competências técnicas para que possa ...
* **T4J:** pretende que a plataforma permita catalogar as competências técnicas e as categorias de tarefas em áreas de atividade.


#### Pré-condições
Existir uma área de atividade cadastrada no sistema.

#### Pós-condições
A informação da competência técnica é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a definição de uma nova competência técnica.
2. O sistema solicita os dados necessários (i.e. código único, descrição breve e detalhada). 
3. O administrativo introduz os dados solicitados.
4. Mostra lista de área de atividade e pede para selecionar uma.
5. O administrativo seleciona área de atividade.
6. O sistema valida e apresenta informações e solicita confirmação.
7. O administrativo confirma. 
8. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da definição da  área de atividade.

> O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

6c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 	1. O sistema alerta o administrativo para o facto. 
> 	2. O sistema permite a sua alteração (passo 3).
> 
	> 2a. O administrativo não altera os dados. O caso de uso termina. 

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
\-

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC4_MD.svg](UC4_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia a definição de uma nova competência técnica.   		 |	... interage com o utilizador? | EspecificarCompetenciaUI    |   |
|  		 |	... coordena o UC?	| EspecificarCompetenciaController | Controller    |
|  		 |	... cria instância de CompetenciaTecnica| Plataforma   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários (i.e. código único, descrição breve e detalhada).  		 |							 |             |                              |
| 3. O administrativo introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   CompetenciaTecnica | Information Expert (IE) - instância criada no passo 1     |
| 4. Mostra lista de área de atividade e pede para selecionar uma.  		 | ...conhece as areas de atividade existentes a listar? | Plataforma   |   IE: Plataforma tem/agrega todas as Areas de Atividade |
| 5. O administrativo seleciona área de atividade.  		 | 	... guarda os dados introduzidos?  |   CompetenciaTecnica | Information Expert (IE) - instância criada no passo 1     |
| 6. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.   		 |	...valida os dados da CT (validação local) | CompetenciaTecnica                               |IE. Possui os seu próprios dados.|  	
|	 |	...valida os dados da CT (validação global) | Plataforma  | IE: A Plataforma possui/agrega CopetenciaTecnica  |
| 7. O administrativo confirma.   		 |							 |             |                              |
| 8. O sistema regista os dados e informa o administrativo do sucesso da operação.  		 |	... guarda a CompetenciaTecnica criada? | Plataforma  | IE: No MD a Plataforma possui CompetenciaTecnica|  
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * CategoriaTarefa
 * CompetenciaTecnica


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarCompetenciaUI
 * EspecificarCompetenciaController


###	Diagrama de Sequência
![UC4_SD.svg](UC4_SD.svg)



###	Diagrama de Classes
![UC4_CD.svg](UC4_CD.svg)






