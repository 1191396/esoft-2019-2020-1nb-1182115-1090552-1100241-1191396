# Glossário

| **_Termo_**                   	| **_Descrição_**                                           |                                       
|:------------------------|:----------------------------------------------------------------|
| **Administrativo** | Pessoa responsável por realizar na plataforma várias atividades de suporte ao negócio.|
| **ADM** | Acrónimo para Administrativo.|
| **Categoria (de Tarefa)** | Corresponde a uma descrição usada para catalogar um ou mais tarefas (semelhantes).|
| **Colaborador da Organização** | Pessoa, espicificada pelo Gestor da Organização, responsável por especificar tarefas.|
| **Competência Técnica** | Conhecientos ou habilidades requeridas para realizar uma tarefa.|
| **CT** | Acrónimo para Competência Técnica.|
| **Gestor da Organização** | Pessoa, indicada aquando do registo da organização na plataforma, sendo o colaborador responsável por especificar na plataforma outros colaboradores da organização.|
| **Organização** | Pessoa coletiva que pretende contratar _freelancers_ para a realização de tarefas necessárias à atividade da mesma.|
| **_Freelancer_** | Pessoa individual que trabalha por conta própria e não está necessariamente comprometida com uma entidade empregadora (organização) específica a longo prazo.|
| **Plataforma** | Programa informático. |
| **Processo de Autenticação** | Meio através do qual se procede à verificação da identidade da pessoa que pretende/está a utilizar a plataforma informática.|
| **Tarefa** | Atividade que precisa ser realizada dentro de um período de tempo definido ou por um prazo.|
| **Utilizador** | Pessoa que interage com a aplicação informática.|
| **Utilizador Não Registado** | Utilizador que interage com a plataforma informática de forma anónima, i.e. sem ter realizado previamente o processo de autenticação previsto.|
| **Utilizador Registado** | Utilizador que interage com a plataforma informática após ter realizado o processo de autenticação previsto e, portanto, a aplicação conhece a sua identidade. Tipicamente, este assume o papel/função de Administrativo ou Gestor de Organização ou Colaborador de Organização ou _Freelancer_.|








