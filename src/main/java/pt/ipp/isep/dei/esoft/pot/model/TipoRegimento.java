/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import pt.ipp.isep.dei.esoft.pot.regimentos.SubjetivaObrigatoria;
import pt.ipp.isep.dei.esoft.pot.regimentos.Automatico2PrecoMaisBaixo;
import pt.ipp.isep.dei.esoft.pot.regimentos.SubjetivaOpcional;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface TipoRegimento {

    
     
     String getDescricao();

     String getRegra();
    
}
