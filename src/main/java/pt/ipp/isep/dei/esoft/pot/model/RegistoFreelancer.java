/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author Marcel - 1191396
 */
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;

public class RegistoFreelancer {
    
    private Freelancer fl;
    private List<Freelancer> listaFreelancer;

    /**
     *
     * @param nome_compl
     * @param NIF
     * @param telefone
     * @param email
     */
    public void novoFreelancer(String nome_compl, int NIF, int telefone, String email) {
        //cria uma nova instância de freelancer.
    }
    
    public void addNovoFreelancer(){
        //adiciona freelancer criado a lista de freelancers
    }
    
    public boolean validaFreelancer(Freelancer fl){
        //validação global instância freelancer
        return false;
    }
    
    public List<Freelancer> getListaFreelancer() {
        return this.listaFreelancer;
    }

    public Freelancer getFreelancerByEmail(String email) {

        for (Freelancer freelancer : listaFreelancer) {
            if (freelancer.getEmail().equals(email)) {
                 return freelancer;   
            }
        }
        return null;

    }

}
