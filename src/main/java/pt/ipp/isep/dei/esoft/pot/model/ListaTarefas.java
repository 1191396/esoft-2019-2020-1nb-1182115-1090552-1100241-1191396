/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.AplicacaoPOT;
import pt.ipp.isep.dei.esoft.pot.model.CategoriaTarefa;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;

/**
 *
 * @author 1100241
 */
public class ListaTarefas {

    List<Tarefa> listaTarefa;
    List<Tarefa> listaTarefaPublicadas;
    List<Tarefa> listaTarefaNaoPublicadas;

    private Plataforma m_oPlataforma;
    
    public ListaTarefas(){
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
        
        
    /**
     *
     * @param design
     * @param descInf
     * @param descTec
     * @param duracao
     * @param custo
     * @param categoria
     */
    public Tarefa novaTarefa(String design, String descInf, String descTec, float duracao, float custo, CategoriaTarefa categoria, Colaborador colaborador) {
        Tarefa tarefa = new Tarefa(design, descInf, descTec, duracao, custo, categoria, colaborador);
        return tarefa;
    }

    /**
     *
     * @param tarefa
     */
    public boolean registaTarefa(Tarefa tarefa) {
        return false;
    }

    /**
     *
     * @param tarefa
     */
    public boolean validaTarefa(Tarefa tarefa) {
        return false;
    }

    /**
     *
     * @param tarefa
     */
    public boolean adicionaTarefa(Tarefa tarefa) {
        return false;
    }

    public List<Tarefa> getTarefasNaoPublicadasByColaborador(Colaborador colaborador) {

        RegistoAnuncio registoAnuncio = this.m_oPlataforma.getRegistoAnuncio();

        for (Tarefa tarefa : listaTarefa) {
            if (colaborador.equals(tarefa.getColaborador())) {
                if (!registoAnuncio.tensAnuncioComCodigoTarefa(tarefa.getCodigo())) {
                    listaTarefaNaoPublicadas.add(tarefa);
                };
            }
        }
        return listaTarefaNaoPublicadas;

    }

    public List<Tarefa> getTarefasPublicadasByEmail(String email) {
        // TODO - implement ListaTarefas.getTarefasPublicadasByEmail
        throw new UnsupportedOperationException();
    }

    public Tarefa getTarefaById(int id) {
        return listaTarefaNaoPublicadas.get(id);
    }
;

}
