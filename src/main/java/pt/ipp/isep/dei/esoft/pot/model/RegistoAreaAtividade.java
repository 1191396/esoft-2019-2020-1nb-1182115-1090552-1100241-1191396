package pt.ipp.isep.dei.esoft.pot.model;


import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;

public class RegistoAreaAtividade {

	private AreaAtividade area;
	private List<AreaAtividade> lsAA;
	private int attribute;

	public AreaAtividade getArea() {
		return this.area;
	}

	/**
	 * 
	 * @param area
	 */
	public void setArea(AreaAtividade area) {
		this.area = area;
	}

	public List<AreaAtividade> getLsAA() {
		return this.lsAA;
	}

	/**
	 * 
	 * @param lsAA
	 */
	public void setLsAA(List<AreaAtividade> lsAA) {
		this.lsAA = lsAA;
	}

}