/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcel - 1191396
 */
public class CompetenciaTecnica {
    

    // Codigo da competencia tecnica
    public String codigo;
    // Descrição Breve da competencia tecnica
    private String descBreve;
    // Descoção Detalhada da competencia tecnica
    private String descDetalhada;
    // Area de Atividade da competencia tecnica
    private AreaAtividade area;
    // Lista de Graus de Proficiencia
    private List<GrauProficiencia> lsgp = new ArrayList();
    // Grau de proficiencia da competencia tecnica
    private GrauProficiencia grau;

    /**
     * Instanciação de uma competencia Tecnica
     *
     * @param codigo
     * @param descBreve
     * @param descDet
     * @param areaAtividade
     */

    public CompetenciaTecnica(String codigo, String descBreve, String descDetalhada, AreaAtividade area) {
        this.codigo = codigo;
        this.descBreve = descBreve;
        this.descDetalhada = descDetalhada;
        this.area =area;
    }

    /**
     * Metodo que verifica se a competencia tecnica já existe
     *
     * @param areaId
     */
    public boolean hasId(int areaId) {
        throw new UnsupportedOperationException();
    }

    /**
     * Instancia um novo Grau de profiencia
     *
     * @param valor
     * @param designacao
     */
    public GrauProficiencia addGrauProficiencia(int valor, String designacao) {
        grau = new GrauProficiencia(valor, designacao);
        lsgp.add(grau);
        return grau;
    }

    /**
     * Devolve a lista de Grau de Profiencia
     *
     * @return
     */
    public List<GrauProficiencia> getGrauProficiencia() {
        return lsgp;
    }

    /**
     * Devolve o codigo da competencia tecnica
     *
     * @return
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define o Grau de proficiencia da competencia tecnica
     *
     * @param grau
     */
    public void setGrauProficiencia(GrauProficiencia grau) {
        this.grau = grau;

    }

    @Override
    public String toString() {
        return String.format("Competencia Tecnica: %s\r\nDescrição Breve: %s\r\n"
                + "Descrição Detalhada: %s\r\nArea de Atividade: %s\r\nGrau de Proficiencia: %s\r\n",
                codigo, descBreve, descDetalhada, area, grau);
    }
}