/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.TipoRegimento;

/**
 *
 * @author Admin
 */
public class RegistoAnuncio {

    List<Anuncio> listaAnuncio;

    public boolean tensAnuncioComCodigoTarefa(String codigo) {
        boolean found = false;
        for (Anuncio anuncio : listaAnuncio) {
            if (codigo.equals(anuncio.getTarefa().getCodigo())) {
                return true;
            }
        }
        return found;
    }

    public boolean validaAnuncio(Anuncio anuncio) {
        return true;
    }

    public Anuncio novoAnuncio(Tarefa tarefa, Date dataInicioPublicao, Date dataFimPublicacao, Date dataInicioCandidatura, Date dataFimCandidatura, Date dataInicioSeriacao, Date dataFimSeriacao, TipoRegimento tipoRegimento) {
        return new Anuncio(tarefa, dataInicioPublicao, dataFimPublicacao, dataInicioCandidatura, dataFimCandidatura, dataInicioSeriacao, dataFimSeriacao, tipoRegimento);
    }

    public List<Anuncio> getAnunciosNaoSeriadosByEmail(String email) {
        return listaAnuncio;
    }

    public Anuncio getAnuncioById(int AnuncioId) {
        return null;
    }

    public void getAnuncioByCodigoTarefa(String codigoTarefa) {

    }

    public boolean validaDataSeriacao() {
        return false;
    }

    public void registaAnuncio(Anuncio anuncio) {

    }

    public List<Candidatura> getListCandidaturasByFreelancer(Freelancer freelancer){
        
        List<Candidatura>  lstCandidaturas = new ArrayList<>();
        for(Anuncio anuncio : listaAnuncio){
            ListaCandidaturas listaCandidaturas = anuncio.getListaCandidaturas();
            Date dataFim = anuncio.getDataFimCandidatura();
            
            Candidatura c = listaCandidaturas.getCandidaturaDeFreelancer(freelancer, dataFim);
            if(c!=null){
                lstCandidaturas.add(c);
            }
            
        }
        
        return lstCandidaturas;
    }

    public boolean retiraCandidatura(Candidatura candidatura){
        
        for(Anuncio anuncio : listaAnuncio){
            ListaCandidaturas listaCandidaturas = anuncio.getListaCandidaturas();
            Date dataFim = anuncio.getDataFimCandidatura();
                        
            if(listaCandidaturas.tensCandidatura(candidatura,dataFim)){
                return anuncio.removerCandidatura(candidatura);
            }
        }
        return false;
    }
}
