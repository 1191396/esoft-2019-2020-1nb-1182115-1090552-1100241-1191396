/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.controller.AplicacaoPOT;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Plataforma {

    private String m_strDesignacao;
    private final AutorizacaoFacade m_oAutorizacao;

    private final Set<AreaAtividade> m_lstAreasAtividade;
    private final Set<CompetenciaTecnica> m_lstCompetencias;
    
    private final RegistoCategorias registoCategoriaTarefa;
    private final RegistoAnuncio registoAnuncio;
    private final RegistoOrganizacao registoOrganizacao;
    private final RegistoFreelancer registoFreelancer;
    private final RegistoTiposRegimento registoTiposRegimento;

    public Plataforma(String strDesignacao) {
        if ((strDesignacao == null)
                || (strDesignacao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strDesignacao = strDesignacao;

        this.m_oAutorizacao = new AutorizacaoFacade();

        this.m_lstAreasAtividade = new HashSet<>();
        this.m_lstCompetencias = new HashSet<>();
        
        registoCategoriaTarefa = new RegistoCategorias();
        registoAnuncio =  new RegistoAnuncio();
        registoOrganizacao = new RegistoOrganizacao();
        registoFreelancer = new RegistoFreelancer();
        registoTiposRegimento = new RegistoTiposRegimento();
    }

    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }

    // </editor-fold>
    // Competências Tecnicas
    // <editor-fold defaultstate="collapsed">
    public CompetenciaTecnica getCompetenciaTecnicaById(int Id) {
        for (CompetenciaTecnica oCompTecnica : this.m_lstCompetencias) {
            if (oCompTecnica.hasId(Id)) {
                return oCompTecnica;
            }
        }

        return null;
    }

    public CompetenciaTecnica novaCompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoCompleta, AreaAtividade oArea) {
        return new CompetenciaTecnica(strId, strDescricaoBreve, strDescricaoCompleta, oArea);
    }

    public boolean registaCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        if (this.validaCompetenciaTecnica(oCompTecnica)) {
            return addCompetenciaTecnica(oCompTecnica);
        }
        return false;
    }

    private boolean addCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        return m_lstCompetencias.add(oCompTecnica);
    }

    public boolean validaCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }

    /**
     *
     * @param competencia
     */
    public void selecionarCompetencia(CompetenciaTecnica competencia) {
        // TODO - implement Plataforma.selecionarCompetencia
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param areaId
     */
    public List<CompetenciaTecnica> getCompetenciasTecnicaByAreaId(int areaId) {
        // TODO - implement Plataforma.getListCompetenciaTecnicaByAreaId
        throw new UnsupportedOperationException();
    }

    // </editor-fold>
    // Areas de Atividade 
    // <editor-fold defaultstate="collapsed">
    public AreaAtividade getAreaAtividadeById(String strId) {
        for (AreaAtividade area : this.m_lstAreasAtividade) {
            if (area.hasId(strId)) {
                return area;
            }
        }

        return null;
    }

    public AreaAtividade novaAreaAtividade(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada) {
        return new AreaAtividade(strCodigo, strDescricaoBreve, strDescricaoDetalhada);
    }

    public boolean registaAreaAtividade(AreaAtividade oArea) {
        if (this.validaAreaAtividade(oArea)) {
            return addAreaAtividade(oArea);
        }
        return false;
    }

    private boolean addAreaAtividade(AreaAtividade oArea) {
        return m_lstAreasAtividade.add(oArea);
    }

    public boolean validaAreaAtividade(AreaAtividade oArea) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }

    public List<AreaAtividade> getAreasAtividade() {
        List<AreaAtividade> lc = new ArrayList<>();
        lc.addAll(this.m_lstAreasAtividade);
        return lc;
    }

    // </editor-fold>
    // Categoria de tarefa
    // <editor-fold defaultstate="collapsed">
    /**
     *
     * @param id
     */
    public List<CategoriaTarefa> getCategoriasTarefa() {
        // TODO - implement Plataforma.getListCategoriaTarefa
        throw new UnsupportedOperationException();
    }

    public CategoriaTarefa getCategoriaById(int id) {
        // TODO - implement Plataforma.getCategoriaById
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param desc
     * @param areaAtividade
     * @param lstCompetenciaTecnica
     */
    public CategoriaTarefa novaCategoriaTarefa(String desc, AreaAtividade areaAtividade, List<CompetenciaTecnica> lstCompetenciaTecnica) {
        // TODO - implement Plataforma.novaCategoriaTarefa
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param categoria
     */
    public boolean registaCategoriaTarefa(CategoriaTarefa categoria) {
        return false;
    }

    /**
     *
     * @param categoria
     */
    public boolean validaCategoriaTarefa(CategoriaTarefa categoria) {
        return false;
    }

    /**
     *
     * @param categoria
     */
    public boolean adicionaCategoriaTarefa(CategoriaTarefa categoria) {
        return false;
    }

    // </editor-fold>
    // Utilizador
    // <editor-fold defaultstate="collapsed">
    public boolean registaUtilizadorComPapel() {
        // TODO - implement Plataforma.registaUtilizadorComPapel
        throw new UnsupportedOperationException();
    }

    public boolean existeUtilizador() {
        // TODO - implement Plataforma.existeUtilizador
        throw new UnsupportedOperationException();
    }
    // </editor-fold>

    //RegistoCategoriaTarefa
    public RegistoCategorias getRegistoCategoriaTarefa() {
        return registoCategoriaTarefa;
    }

    //Get email By Login
    public static String getEmailByLogin() {
        AplicacaoPOT aplicacaoPOT = AplicacaoPOT.getInstance();
        SessaoUtilizador sessao = aplicacaoPOT.getSessaoAtual();
        return sessao.getEmailUtilizador();
    }
    
    //Get organizacao by email utilizador
    public Organizacao getOrganizacaoByEmailUtilizador(String email) {
        
        Organizacao orgReturn = null;
        
        ListaColaboradores registoColaborador;
        
        for (Organizacao org : registoOrganizacao.getListOrganizacao()) {
            
            registoColaborador = org.getListaColaboradores();
            
            if (registoColaborador.tensColaboradorComEmail(email)) {
                return org;
            }
        }
        return orgReturn;
    }
    
    //RegistoAnuncio
    public RegistoAnuncio getRegistoAnuncio() {
        return registoAnuncio;
    }

    //RegistoOrganizacao
    public RegistoOrganizacao getRegistoOrganizacao() {
        return registoOrganizacao;
    }
    
    //RegistoFreelancer
    public RegistoFreelancer getRegistoFreelancer(){
        return registoFreelancer;
    }
    
        //RegistoTipoRegimento
    public RegistoTiposRegimento getRegistoTiposRegimento(){
        return registoTiposRegimento;
    }

}
