/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.EnderecoPostal;

/**
 *
 * @author Admin
 */
public class RegistoOrganizacao {
    
    private final AutorizacaoFacade m_oAutorizacao;
    private final Set<Organizacao> m_lstOrganizacoes;
    
    
    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }
    
    public RegistoOrganizacao(){
        this.m_oAutorizacao = new AutorizacaoFacade();
        this.m_lstOrganizacoes = new HashSet<>();
    }
    
    public Organizacao novaOrganizacao(String strNome, String strNIF, String strWebsite, String strTelefone, String strEmail, EnderecoPostal oMorada, Colaborador oGestor) {
        return new Organizacao(strNome, strNIF, strWebsite, strTelefone, strEmail, oMorada, oGestor);
    }

    public boolean registaOrganizacao(Organizacao oOrganizacao, String strPwd) {
        if (this.validaOrganizacao(oOrganizacao, strPwd)) {
            Colaborador oGestor = oOrganizacao.getGestor();
            String strNomeGestor = oGestor.getNome();
            String strEmailGestor = oGestor.getEmail();
            if (this.m_oAutorizacao.registaUtilizadorComPapeis(strNomeGestor, strEmailGestor, strPwd,
                    new String[]{Constantes.PAPEL_GESTOR_ORGANIZACAO, Constantes.PAPEL_COLABORADOR_ORGANIZACAO})) {
                return addOrganizacao(oOrganizacao);
            }
        }
        return false;
    }

    private boolean addOrganizacao(Organizacao oOrganizacao) {
        return m_lstOrganizacoes.add(oOrganizacao);
    }

    public boolean validaOrganizacao(Organizacao oOrganizacao, String strPwd) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        if (this.m_oAutorizacao.existeUtilizador(oOrganizacao.getGestor().getEmail())) {
            bRet = false;
        }
        if (strPwd == null) {
            bRet = false;
        }
        if (strPwd.isEmpty()) {
            bRet = false;
        }
        //

        return bRet;
    }

    public Set<Organizacao> getListOrganizacao(){
        return m_lstOrganizacoes;
    }
    
}
