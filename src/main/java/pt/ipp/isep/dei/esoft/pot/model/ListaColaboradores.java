/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;

/**
 *
 * @author 1100241
 */
public class ListaColaboradores {

    private List<Colaborador> m_lstColaboradores;

    public List<Colaborador> getListaColaboradores() {
        return this.m_lstColaboradores;
    }

    /**
     *
     * @param colaboradorId
     */
    public Colaborador getColaboradorById(int colaboradorId) {
        // TODO - implement ListaColaboradores.getColaboradorById
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param colab
     */
    public boolean adicionaColaborador(Colaborador colab) {
        return false;
    }

    /**
     *
     * @param colab
     */
    public boolean registaColaborador(Colaborador colab) {
        return false;
    }

    /**
     *
     * @param colab
     */
    public boolean validaColaborador(Colaborador colab) {
        return false;
    }

    public boolean tensColaboradorComEmail(String email) {

        for (Colaborador c : m_lstColaboradores) {
            if (email.equals(c.getEmail())) {
                return true;
            }
        }
        return false;
    }
    
    public Colaborador getColaboradorByEmail(String email){
        
        for (Colaborador c : m_lstColaboradores) {
            if (email.equals(c.getEmail())) {
                return c;
            }
        }
      return null;
    }
}
