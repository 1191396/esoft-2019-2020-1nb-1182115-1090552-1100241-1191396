/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.CategoriaTarefa;

/**
 *
 * @author Admin
 */
public class RegistoCategorias {

    private List<CategoriaTarefa> listaCategoriaTarefa;

    public List<CategoriaTarefa> getListCategoriaTarefa() {
        return listaCategoriaTarefa;
    }
    
    public CategoriaTarefa getCategoriaTarefaById(int id){
        return listaCategoriaTarefa.get(id);
    }

}
