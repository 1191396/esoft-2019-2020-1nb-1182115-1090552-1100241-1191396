/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author Marcel - 1191396
 */
import java.util.Date;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.GrauProficiencia;

/**
 *
 * @author Marcel 1191396
 */
public class ReconhecimentoTecnico {
    
    //Data em que foi conferido o reconhecimento da competência técnia
    private Date dtReconhecimento;
    
    //Competência Técnica reconhcida
    private CompetenciaTecnica ct;
    
    //Grau de proficiência reconhecido
    private GrauProficiencia gp;
    
    /**
     *Constrói o objeto tipo ReconhecimentoCompetenciaTecnica 
     * @param dtReconhecimento
     * @param ct
     * @param gp 
     */
    public ReconhecimentoTecnico(Date dtReconhecimento, CompetenciaTecnica ct, GrauProficiencia gp){
       this.dtReconhecimento = dtReconhecimento;
       this.ct = ct;
       this.gp = gp;
    }
    
    /**
     * Devolve o Grau de Proficiencia 
     * @return 
     */
    public GrauProficiencia getGrauProficiencia(){
        return gp;
    }
    
     @Override
    public String toString() { return String.format("Reconhecimento Competencia Tecnica \nDate: %s - %s\r",
                dtReconhecimento, gp);
    }
}
