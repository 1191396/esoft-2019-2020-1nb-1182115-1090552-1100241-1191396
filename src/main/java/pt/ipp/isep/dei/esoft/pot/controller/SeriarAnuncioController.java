/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;
import pt.ipp.isep.dei.esoft.pot.model.ListaTarefas;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;

/**
 *
 * @author danie
 */
public class SeriarAnuncioController {

    private Plataforma m_oPlataforma;

    public SeriarAnuncioController() {

        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO)) {
            throw new IllegalStateException("Utilizador não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();

    }

    public void getListAnuncios() {

        String email = Plataforma.getEmailByLogin();
        Organizacao org = this.m_oPlataforma.getOrganizacaoByEmailUtilizador(email);
        RegistoAnuncio registoAnuncio = this.m_oPlataforma.getRegistoAnuncio();
        ListaTarefas registoTarefa = org.getListaTarefas();
        List<Tarefa> tarefas = registoTarefa.getTarefasPublicadasByEmail(Plataforma.getEmailByLogin());

    }

    /**
     *
     * @param anuncioId
     */
    public void getAnuncioById(int anuncioId) {
        // TODO - implement SeriarAnuncioController.getAnuncioById
        throw new UnsupportedOperationException();
    }

    public void getCandidaturaById() {
        // TODO - implement SeriarAnuncioController.getCandidaturaById
        throw new UnsupportedOperationException();
    }

    public void addColaboradorAListaParticipantes() {
        // TODO - implement SeriarAnuncioController.addColaboradorAListaParticipantes
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param colaboradorID
     */
    public void getColaboradorById(int colaboradorID) {
        // TODO - implement SeriarAnuncioController.getColaboradorById
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param candidaturaId
     * @param posicao
     */
    public void novaClassificacao(String candidaturaId, int posicao) {
        // TODO - implement SeriarAnuncioController.novaClassificacao
        throw new UnsupportedOperationException();
    }
}
