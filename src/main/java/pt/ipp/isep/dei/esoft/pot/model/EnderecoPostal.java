/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author Marcel - 1191396
 */
public class EnderecoPostal {

	private String local;
	private String codPostal;
	private String localidade;

	/**
	 * 
	 * @param endLocal
	 * @param endCodPostal
	 * @param EndLocalidade
	 */
	public EnderecoPostal(String endLocal, String endCodPostal, String EndLocalidade) {
             this.local = endLocal;
             this.codPostal = endCodPostal;
             this.localidade = EndLocalidade;
	}

}