/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Date;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;
import pt.ipp.isep.dei.esoft.pot.model.ListaColaboradores;
import pt.ipp.isep.dei.esoft.pot.model.ListaTarefas;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoTiposRegimento;
import pt.ipp.isep.dei.esoft.pot.regimentos.SubjetivaOpcional;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.model.TipoRegimento;

/**
 *
 * @author 1100241
 */
public class PublicarTarefaController {

    private Plataforma m_oPlataforma;
    
    public PublicarTarefaController(){
        
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
    
    public void getListTarefas() {

        String email = Plataforma.getEmailByLogin();
        Organizacao org = this.m_oPlataforma.getOrganizacaoByEmailUtilizador(email);
        ListaColaboradores listaColaboradores = org.getListaColaboradores();
        Colaborador colaborador = listaColaboradores.getColaboradorByEmail(email);
        ListaTarefas listaTarefas = org.getListaTarefas();
        List<Tarefa> tarefas = listaTarefas.getTarefasNaoPublicadasByColaborador(colaborador);

    }

    public void getTarefaById(int id) {
        String email = Plataforma.getEmailByLogin();
        Organizacao org = this.m_oPlataforma.getOrganizacaoByEmailUtilizador(email);
        ListaTarefas listaTarefas = org.getListaTarefas();
        listaTarefas.getTarefaById(id);
    }

    public  void getTipoRegimentoById(int id) {
        RegistoTiposRegimento registoTiposRegimento = this.m_oPlataforma.getRegistoTiposRegimento();
        TipoRegimento tipoRegimento = registoTiposRegimento.getTipoRegimentoById(id);
    }

    public void novoAnuncio(Tarefa tarefa) {
        
        Date dataInicioPublicao = new Date();
        Date dataFimPublicacao= new Date();
        Date dataInicioCandidatura= new Date();
        Date dataFimCandidatura= new Date();
        Date dataInicioSeriacao= new Date();
        Date dataFimSeriacao= new Date();
        TipoRegimento tipoRegimento = new SubjetivaOpcional();

        RegistoAnuncio registoAnuncio = this.m_oPlataforma.getRegistoAnuncio();

        Anuncio anuncio = registoAnuncio.novoAnuncio(tarefa, dataInicioPublicao, dataFimPublicacao, dataInicioCandidatura, dataFimCandidatura, dataInicioSeriacao, dataFimSeriacao, tipoRegimento);
        
        registoAnuncio.registaAnuncio(anuncio);
    }
}
