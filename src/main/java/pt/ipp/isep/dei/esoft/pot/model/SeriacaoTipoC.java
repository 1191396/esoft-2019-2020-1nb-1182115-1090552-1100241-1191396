/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author Admin
 */
public class SeriacaoTipoC implements TipoRegimento{

    String descricao = "Seriação e atribuição automática com base no segundo preço mais baixo";
    String regra = "Atribuição assenta exclusivamente no preço apresentado pelos candidatos";
    
    @Override
    public String getDescricao() {
        return descricao;
    }

    @Override
    public String getRegra() {
        return regra;
    }
    
    
}
