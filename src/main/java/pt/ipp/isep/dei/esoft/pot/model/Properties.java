/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 *
 * @author 1100241
 */
public class Properties{

    public Properties readProperties() throws FileNotFoundException {
        File fichConfiguracao = new File("config.properties");
        FileReader reader = new FileReader(fichConfiguracao);
        Properties props = new Properties();
        props.load(reader);
        
        return props;
    }
    
    public void load(FileReader reader){
        
    }
}
