/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.ListaColaboradores;
import pt.ipp.isep.dei.esoft.pot.model.ListaTarefas;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;
import pt.ipp.isep.dei.esoft.pot.model.RegistoFreelancer;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;

/**
 *
 * @author 1182115
 */
public class AtualizarCandidaturaController {

    private Plataforma m_oPlataforma;
    private List<Candidatura> lstCandidaturas;
    private Candidatura candidatura;
    
    public RetirarCandidaturaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO)) {
            throw new IllegalStateException("Utilizador não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }

    public Candidatura getCandidaturaById(int id) {
        return candidatura = lstCandidaturas.get(id);
    }

    public List<Candidatura> getListCandidaturas() {

        String email = Plataforma.getEmailByLogin();
        RegistoFreelancer registoFreelancer = this.m_oPlataforma.getRegistoFreelancer();
        RegistoAnuncio registoAnuncio = this.m_oPlataforma.getRegistoAnuncio();
        Freelancer freelancer = registoFreelancer.getFreelancerByEmail(email);

        lstCandidaturas = registoAnuncio.getListCandidaturasByFreelancer(freelancer);
        return lstCandidaturas;

    }

    public List<Anuncio> getListAnuncios() {
		return null;
    }
	public Candidatura getCandidaturaByAnuncio() {
		return null;
    }
	public void atualizarCanidaturaCopy(double valor, int dias, String apres, String motiv) {

    }
	public void confirmaAtualizacao(){
}
}
