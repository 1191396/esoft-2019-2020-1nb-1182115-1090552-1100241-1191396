/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author Marcel - 1191396
 */
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Freelancer {

	private String nome;
	private int NIF;
	private int telefone;
	private String email;
	private EnderecoPostal endP;
	private List<HabilitacaoAcademica> lsHab = new ArrayList<HabilitacaoAcademica>();
        private ReconhecimentoTecnico rct;
        //Lista de Reconhecimentos de Competência Técnica
        private List<ReconhecimentoTecnico> lsRCT = new ArrayList();

    
    /**
     * Instancia uma novo Freelancer
     * @param nome
     * @param NIF
     * @param telefone
     * @param email 
     */
    public Freelancer(String nome, int NIF, int telefone, String email, EnderecoPostal endP){
        this.nome = nome;
        this.NIF = NIF;
        this.telefone = telefone;
        this.email = email;
        this.endP=endP;
    }
    
    /**
     * Devolve o nome do Freelancer
     * @return 
     */
    public String getNome() {
        return this.nome;
    }
/**
 * Devolve o E-mail do Freelancer
 * @return 
 */
    public String getEmail() {
        return this.email;
    }
    /**
     * Devolve a lista de Reconhecimento competencia Tecnica 
     * @return 
     */
    public List<ReconhecimentoTecnico> getListaReconhecimentoCompetenciaTecnica(){
        return lsRCT;
    }
    
    /**
     * Adiciona novos elementos à Lista de Reconhecimento Competencia Tecnica
     * @param lsRCT 
     */
    
    public void setListaRCT(List<ReconhecimentoTecnico> lsRCT){
        if(validaRCT())
            this.lsRCT.addAll(lsRCT);
    }
    /**
     * Instancia um novo objeto Reconhecimento Competencia Tecnica
     * @param data
     * @param comptec
     * @param obj_gp 
     */
    public void createRCT(Date data, CompetenciaTecnica comptec,GrauProficiencia obj_gp){
        rct = new ReconhecimentoTecnico(data, comptec, obj_gp);
    }
    /**
     * Adiciona à lista uma nova Reconhecimento Competencia Tecnica
     */
    public void addRCT(){
//        if(validaRCT())
            lsRCT.add(rct);
    }
    /**
     * Valida o objeto Reconhecimento Competencia Tecnica
     * @return 
     */
    public boolean validaRCT(){
        return lsRCT.contains(rct);
    }
	public void addHabilitacaoAcademica(String grau, String designacao, String instituicao, float media) {
            //adiciona a habilitação acadêmica a lista de habilitações
	}

	/**
	 * 
	 * @param hab
	 */
	public void validaHabilitacaoAcademica(HabilitacaoAcademica hab) {
            //realiza a validação da habilitação acadêmica
	}

}