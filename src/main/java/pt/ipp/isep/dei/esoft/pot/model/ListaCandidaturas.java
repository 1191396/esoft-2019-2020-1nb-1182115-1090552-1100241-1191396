package pt.ipp.isep.dei.esoft.pot.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;

public class ListaCandidaturas {

    private List<Candidatura> listaCandidaturas;

    public Candidatura getCandidaturaById() {
        // TODO - implement ListaCandidaturas.getCandidaturaById
        throw new UnsupportedOperationException();
    }

    public List<Candidatura> getListCandidatura() {
        // TODO - implement ListaCandidaturas.getListCandidatura
        throw new UnsupportedOperationException();
    }

    public List<Candidatura> getListaCandidaturaOrdenada() {
        // TODO - implement ListaCandidaturas.getListaCandidaturaOrdenada
        throw new UnsupportedOperationException();
    }

    public Candidatura getCandidaturaDeFreelancer(Freelancer freelancer, Date dataLimiteCandidatura) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dataAtual = new Date(System.currentTimeMillis());

        for (Candidatura c : listaCandidaturas) {

            if (c.getFreelancer().equals(freelancer) && dataAtual.before(dataLimiteCandidatura)) {
                return c;
            }

        }
        return null;
    }
    
    public boolean tensCandidatura(Candidatura candidatura, Date dataLimiteCandidatura) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dataAtual = new Date(System.currentTimeMillis());

        for (Candidatura c : listaCandidaturas) {

            if (candidatura.equals(c) && dataAtual.before(dataLimiteCandidatura)) {
                return true;
            }

        }
        return false;
    }
    
    public boolean removerCandidatura(Candidatura candidatura){
        return listaCandidaturas.remove(candidatura);
    }
}
