/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author Admin
 */
public class SeriacaoTipoA implements TipoRegimento {

    String descricao = "Seriação subjetiva com atribuição opcional";
    String regra = "Por definir";

    @Override
    public String getDescricao() {
        return descricao;
    }

    @Override
    public String getRegra() {
        return regra;
    }

}
