/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;

/**
 *
 * @author Admin 
 */
public class Anuncio {

    String codigoAnuncio;
    Date dataInicioPublicao;
    Date dataFimPublicacao;
    Date dataInicioCandidatura;
    Date dataFimCandidatura;
    Date dataInicioSeriacao;
    Date dataFimSeriacao;
    TipoRegimento tipoRegimento;
    Tarefa tarefa;

    ListaCandidaturas listaCandidaturas;

    public Anuncio(Tarefa tarefa, Date dataInicioPublicao, Date dataFimPublicacao, Date dataInicioCandidatura, Date dataFimCandidatura, Date dataInicioSeriacao, Date dataFimSeriacao, TipoRegimento tipoRegimento) {

        if (validaPeriodos(dataInicioPublicao, dataFimPublicacao, dataInicioCandidatura, dataFimCandidatura, dataInicioSeriacao, dataFimSeriacao)) {
            this.dataInicioPublicao = dataInicioPublicao;
            this.dataFimPublicacao = dataFimPublicacao;
            this.dataInicioCandidatura = dataInicioCandidatura;
            this.dataFimCandidatura = dataFimCandidatura;
            this.dataInicioSeriacao = dataInicioSeriacao;
            this.dataFimSeriacao = dataFimSeriacao;
            this.tarefa = tarefa;
            this.tipoRegimento = tipoRegimento;
            listaCandidaturas = new ListaCandidaturas();
        };

    }

    public Tarefa getTarefa() {
        return tarefa;
    }

    public ListaCandidaturas getListaCandidaturas() {
        // TODO - implement Anuncio.getListaCandidaturas
        throw new UnsupportedOperationException();
    }

    public RegistoClassificacao getRegistoClassificacao() {
        // TODO - implement Anuncio.getRegistoClassificacao
        throw new UnsupportedOperationException();
    }

    public void generateCodigoAnuncio() {

    }

    public boolean validaPeriodos(Date dataInicioPublicao, Date dataFimPublicacao, Date dataInicioCandidatura, Date dataFimCandidatura, Date dataInicioSeriacao, Date dataFimSeriacao) {

        if (dataInicioPublicao.after(dataFimPublicacao) || dataInicioCandidatura.after(dataFimCandidatura) || dataInicioSeriacao.after(dataFimSeriacao)) {
            return false;
        }

        if (dataInicioCandidatura.after(dataInicioPublicao) && dataInicioCandidatura.before(dataFimPublicacao)) {
            if (dataInicioSeriacao.after(dataFimPublicacao) && dataInicioSeriacao.after(dataFimPublicacao)) {
                return true;
            }
        }

        return false;
    }

    public Date getDataFimCandidatura() {
        return dataFimCandidatura;
    }
    
    public boolean removerCandidatura(Candidatura candidatura){
        return listaCandidaturas.removerCandidatura(candidatura);
    }
	//1182115

	public Candidatura getCandidaturaByAnuncio(Anuncio anu, Freelancer freel){
	return null; 
	}
	public Candidatura atualizarCandidaturaCopy(double valor, int dias, String aprs, String motiv){
	return null; 
	}

	public boolean confirmaAtualizacao(Candidatura copiaCand){
	return null;
}

}
