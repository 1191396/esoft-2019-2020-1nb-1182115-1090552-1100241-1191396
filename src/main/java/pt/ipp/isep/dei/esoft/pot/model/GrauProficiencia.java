/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;
/**
 * 
 * @author Marcel 1191396
 */
public class GrauProficiencia {
    // Valor do Grau de Proficiencia
    private int valor;
    // Designação do Grau de Proficiencia
    private String designacao;
    //Competencia Tecnica relacioanda com Grau de Proficiencia
    private CompetenciaTecnica comp;

    /**
     * Instancia de Grau de Proficiencia
     * @param valor
     * @param designacao 
     */

    GrauProficiencia(int valor, String designacao) {
       this.valor = valor;
        this.designacao = designacao;
        
    }
/**
 * Devolve o Valor do Grau de Proficiencia
 * @return 
 */
    public int getValor() {
        return valor;
    }
    /**
     * Devolve a Desinação do Grau de Proficiencia
     * @return 
     */
    public String getDesignacao(){
        
        return designacao;
    }

    @Override
    public String toString() {
        return String.format("Grau proficiencia \nDescrição:%s| Valor: %d",
                designacao, valor);
    }
}
