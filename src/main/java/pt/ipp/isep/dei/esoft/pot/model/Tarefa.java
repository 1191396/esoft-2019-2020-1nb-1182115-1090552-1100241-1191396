package pt.ipp.isep.dei.esoft.pot.model;

import pt.ipp.isep.dei.esoft.pot.model.CategoriaTarefa;

public class Tarefa {

	private String codigo;
	private String design;
	private String descInf;
	private String descTec;
	private float duracao;
	private float custo;
        private Colaborador colaborador;

    /**
     *
     * @param codigo
     * @param design
     * @param descInf
     * @param descTec
     * @param duracao
     * @param custo
     * @param colaboradorEmail
     */
    public Tarefa( String design, String descInf, String descTec, float duracao, float custo, CategoriaTarefa categoriaTarefa, Colaborador colaborador) {
        this.codigo = codigo;
        this.design = design;
        this.descInf = descInf;
        this.descTec = descTec;
        this.duracao = duracao;
        this.custo = custo;
        this.colaborador = colaborador;
    }


    public Colaborador getColaborador() {
        return colaborador;
    }

    public String getCodigo() {
        return codigo;
    }

    public void publicar(Anuncio anuncio){
       
        //gera codigo do anuncio
        anuncio.generateCodigoAnuncio();
        
        //publica
       
    }    

}