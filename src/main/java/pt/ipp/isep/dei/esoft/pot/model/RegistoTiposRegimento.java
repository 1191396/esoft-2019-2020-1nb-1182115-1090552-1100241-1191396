/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import pt.ipp.isep.dei.esoft.pot.controller.AplicacaoPOT;

/**
 *
 * @author 1100241
 */
public class RegistoTiposRegimento {

    List<TipoRegimento> listaTipoRegimento;

    public List<TipoRegimento> getListTipoRegimento() throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        if(listaTipoRegimento==null){
            criaTiposRegimentosSuportados(AplicacaoPOT.getProperties());
        }
        return listaTipoRegimento;

    }

    public TipoRegimento getTipoRegimentoById(int id) {
        return listaTipoRegimento.get(id);
    }

    public void criaTiposRegimentosSuportados(Properties props) throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        listaTipoRegimento = new ArrayList<TipoRegimento>();

        //Conhecer quantos tipos de regimento são suportados
        String qtdTipos = props.getProperty("Plataforma.QuantidadeTiposRegimentoSuportados");
        int qtd = Integer.parseInt(qtdTipos);

        //Por cada tipo suportado criar a instância respetiva
        for (int i = 0; i <= qtd; i++) {
            String desc = props.getProperty("Plataforma.TipoRegimento." + i + ".Designacao");
            String classe = props.getProperty("Plataforma.TipoRegimento." + i + ".Classe");

            Class<?> oClass = Class.forName(classe);
            TipoRegimento tipoRegimento = (TipoRegimento) oClass.newInstance();

            listaTipoRegimento.add(tipoRegimento);
            
        }

    }
}
