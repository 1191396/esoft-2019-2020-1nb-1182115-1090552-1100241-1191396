package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.RegistoCategorias;
import pt.ipp.isep.dei.esoft.pot.model.CategoriaTarefa;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;

public class EspecificarTarefaController {

    List<CategoriaTarefa> listaCategoriaTarefas;
    private Plataforma m_oPlataforma;

    
    public EspecificarTarefaController(){
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
    
    public void getListCategoriaTarefa() {
        RegistoCategorias registoCategoriaTarefa = this.m_oPlataforma.getRegistoCategoriaTarefa();
        listaCategoriaTarefas = registoCategoriaTarefa.getListCategoriaTarefa();
    }

    /**
     *
     * @param design
     * @param descInf
     * @param descTec
     * @param duracao
     * @param custo
     * @param catId
     */
    public boolean novaTarefa(String design, String descInf, String descTec, float duracao, float custo, int catId) {
        return false;
    }

    public boolean registaTarefa() {
        return false;
    }

    /**
     *
     * @param codigo
     * @param descBreve
     * @param descDet
     * @param areaAtividadeId
     */
    public void novaCompetenciaTecnica(String codigo, String descBreve, String descDet, int areaAtividadeId) {
        // TODO - implement EspecificarTarefaController.novaCompetenciaTecnica
        throw new UnsupportedOperationException();
    }

}
